﻿//input
//      5
//     / \
//    3   6
//   / \   \ 
//  1   4   9
var root = 
    new Node {Value=5, 
        Left=new Node{Value=3, 
            Left=new Node{Value=1},
            Right=new Node{Value=4}},
        Right=new Node{Value=6, 
            Right=new Node{Value=9}}};
outputTree(root);

BSTOperations.insertNode(root, 2);
outputTree(root);
//      5
//     / \
//    3   6
//   / \   \ 
//  1   4   9
//   \
//    2

BSTOperations.removeNode(root, 3);
outputTree(root);
//      5
//     / \
//    2   6
//   / \   \
//  1   4   9

BSTOperations.removeNode(root, 4);
outputTree(root);
//      5
//     / \
//    2   6
//   /     \
//  1       9

BSTOperations.removeNode(root, 6);
outputTree(root);
//      5
//     / \
//    2   9
//   /     
//  1       

static void outputTree(Node node) {
    outputTreeInOrder(node);
    Console.WriteLine();
}

static void outputTreeInOrder(Node node) {
    if (node.Left != null) {
        outputTreeInOrder(node.Left);
    }
    Console.Write(node.Value + " ");
    if (node.Right != null) {
        outputTreeInOrder(node.Right);
    }
}