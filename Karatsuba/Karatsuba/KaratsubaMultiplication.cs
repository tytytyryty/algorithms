﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Karatsuba
{
    public class KaratsubaMultiplication
    {
        private int count = 0;

        //x*y = 10^(n)*ac + 10^(n/2)*((a+b)*(c+d)-ac-bd) + bd
        public string Multiply(string number1, string number2)
        {
            if (number1.Length == 1 && number2.Length == 1)
            {
                return (int.Parse(number1) * int.Parse(number2)).ToString();
            }

            MakeNumbersPaired(ref number1, ref number2);

            var n = number1.Length;

            var a = number1.Substring(0, n / 2);
            var b = number1.Substring(n / 2, n / 2);
            var c = number2.Substring(0, n / 2);
            var d = number2.Substring(n / 2, n / 2);

            var ac = Multiply(a, c);
            var bd = Multiply(b, d);
            //(a+b)*(c+d)
            var abcd = Multiply(
                BigInteger.Add(BigInteger.Parse(a), BigInteger.Parse(b)).ToString(),
                BigInteger.Add(BigInteger.Parse(c), BigInteger.Parse(d)).ToString());
            //(a+b)*(c+d)-ac-bd
            var adbc = BigInteger.Subtract(BigInteger.Parse(abcd), BigInteger.Add(BigInteger.Parse(ac), BigInteger.Parse(bd)));

            var result = BigInteger.Add(BigInteger.Add(
                    BigInteger.Multiply(BigInteger.Pow(10, n), BigInteger.Parse(ac)),
                    BigInteger.Multiply(BigInteger.Pow(10, n / 2), adbc)),
                    BigInteger.Parse(bd));

            return result.ToString();
        }

        private void MakeNumbersPaired(ref string number1, ref string number2)
        {
            int maxLength = number1.Length > number2.Length ? number1.Length : number2.Length;
            int pairedNumbersLength = maxLength;
            if (maxLength % 2 != 0)
            {
                pairedNumbersLength = maxLength + 1;
            }

            number1 = number1.PadLeft(pairedNumbersLength, '0');
            number2 = number2.PadLeft(pairedNumbersLength, '0');
        }
    }
}
