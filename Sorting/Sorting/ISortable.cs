﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    interface ISortable<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> array);
    }

    interface ISortable
    {
        IEnumerable<int> Sort(IEnumerable<int> array);
    }
}
