﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using HashTable.HashFunctions;

namespace HashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            //var array = new long[] { 3, 6, 8, 4, 1, 38, 22, 12, 41, 5, 25, 15, 435, 23, 125, 251, 533, 111, 55, 66, 78, 34, 44, 117 };
            
            //Console.WriteLine(Math.Round(hashTable._hashSet.Where(i => i != null).Count() * 1.0/array.Length, 3));
            //Console.Read();

            var d = new Dictionary<string, long>();
            var h = new HashSet<long>();

            var array = File.ReadAllLines("test_10k.txt").Select(i => long.Parse(i)).ToArray();

            //var myHashSet = new ChainingHashSet(Convert.ToInt64(array.Length*2), HashFunctionType.Multiply);
            var myHashSet = new OpenAddressingHashSet(Convert.ToInt64(array.Length * 2), new DoubleHashingFunction());

            var startTtime = DateTime.Now;
            //var sums = searchForSumsHashSet(array, new Interval(-1000, 1000));
            var sums = searchForSumsMyHashSet(myHashSet, array, new Interval(-1000, 1000));
            var endTime = DateTime.Now;

            Console.WriteLine("Count: " + sums.Count);
            Console.WriteLine("Total seconds: " + (endTime - startTtime).TotalSeconds);

            Console.WriteLine(myHashSet.ToString());
            
            Console.ReadLine();
        }        

        private static List<long> searchForSumsMyHashSet(IHashSet hashSet, long[] array, Interval interval)
        {
            var result = new List<long>();

            foreach (var item in array)
            {
                hashSet.Add(item);
            }

            for (int i = 0; i < array.Length; i++)
            {
                for (int sum = interval.From; sum <= interval.To; sum++)
                {
                    var x = array[i];

                    if (hashSet.Contains(sum - x) && !result.Contains(sum))
                    {
                        result.Add(sum);
                    }
                }
            }
            return result;
        }

        private static List<long> searchForSumsHashSet(long[] array, Interval interval)
        {
            var result = new List<long>();

            var hashSet = new HashSet<long>(array);
            //foreach (var item in array)
            //{
            //    hashSet.Add(item);
            //}
            
            for (int i = 0; i < array.Length; i++)
            {
                for (int sum = interval.From; sum <= interval.To; sum++)
                {
                    var x = array[i];

                    if (hashSet.Contains(sum - x) && !result.Contains(sum))
                    {
                        result.Add(sum);
                    }
                }
            }
            return result;
        }

        
    }
}
