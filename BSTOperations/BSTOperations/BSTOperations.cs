public class BSTOperations {
    public static void insertNode(Node root, int value) {
        if (value > root.Value) {
            if (root.Right != null) {
                insertNode(root.Right, value);
            } else {
                root.Right = new Node() {Value=value};
            }
        } else {
            if (root.Left != null) {
                insertNode(root.Left, value);
            } else {
                root.Left = new Node() {Value=value};
            }
        }
    }

    public static void removeNode(Node root, int value) {
        removeNodeRecursive(root, value, null);
    }

    public static void removeNodeRecursive(Node root, int value, Node previous) {
        if (root == null) {
            Console.WriteLine("Node with such value was not found in a tree");
        }
        if (value > root.Value) {
            removeNodeRecursive(root.Right, value, root);
        }
        if (value < root.Value) {
            removeNodeRecursive(root.Left, value, root);
        }
        if (value == root.Value) {
            if (root.Left == null && root.Right == null) {
                assignToPrevious(root, previous, null);
            }
            else {
                if (root.Left == null) {
                    assignToPrevious(root, previous, root.Right);
                } else if (root.Right == null) {
                    assignToPrevious(root, previous, root.Left);
                } else {
                    var leftOfDeleted = root.Left;
                    var rightOfDeleted = root.Right;
                    var replacement = getAndRemoveMaxNodeFromSubtree(root.Left, root);
                    assignToPrevious(root, previous, replacement);
                    replacement.Left = leftOfDeleted;
                    replacement.Right = rightOfDeleted;
                }
            }
        }
    }

    private static void assignToPrevious(Node current, Node previous, Node assign) {
        if (current.Value > previous.Value) {
            previous.Right = assign;
        } else {
            previous.Left = assign;
        }
    }

    private static Node getAndRemoveMaxNodeFromSubtree(Node root, Node previous) {
        if (root.Right != null) {
            return getAndRemoveMaxNodeFromSubtree(root.Right, root);
        }
        previous.Right = null;
        return root;
    }
}

public class Node {
    public Node Left { get; set; }
    public Node Right { get; set; }
    public int Value { get; set; }
}