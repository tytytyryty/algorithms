﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorting
{
    class QuickSort : ISortable
    {
        private int _count = 0;
        private int _countCall = 0;

        public IEnumerable<int> Sort(IEnumerable<int> array)
        {
            var arrayToSort = array.ToArray();
            QuickSortRun(ref arrayToSort, 0, arrayToSort.Length - 1);
            return arrayToSort;
        }

        public void QuickSortRun(ref int[] arr, int p, int q)
        {
            if (p < q)
            {
                int r = Partition(ref arr, p, q);
                QuickSortRun(ref arr, p, r - 1);
                QuickSortRun(ref arr, r + 1, q);
            }
        }

        public int Partition(ref int[] arr, int p, int q)
        {
            _countCall++;
            _count += q - p;

            int first = p;
            int last = q;
            int middle = (p + q)/2;

            int median = first;

            if (arr[first] < arr[middle])
            {
                if (arr[middle] < arr[last])
                {
                    median = middle;    
                }
                else
                {
                    if (arr[first] < arr[last])
                    {
                        median = last;
                    }
                    else
                    {
                        median = first;
                    }
                }
            }
            else
            {
                if (arr[first] < arr[last])
                {
                    median = first;
                }
                else
                {
                    if (arr[middle] < arr[last])
                    {
                        median = last;
                    }
                    else
                    {
                        median = middle;
                    }
                }
            }

            var pivot = arr[median];
            Swap(ref arr, q, median);

            //[<p]i[>p]j[?][p]
            //1 3 4 [2]
            //1 2 4 3
            //int i = p - 1;
            //for (int j = i + 1; j < q; j++)
            //{
            //    if (arr[j] <= pivot)
            //    {
            //        i++;
            //        Swap(ref arr, i, j);
            //    }
            //}
            //Swap(ref arr, q, i + 1);
            //return i + 1;

            //[p][<p]i[>p]j[?]
            //[2] 1 3 4 
            //1 2 3 4
            int i = p + 1;
            for (int j = i; j <= q; j++)
            {
                if (arr[j] < pivot)
                {
                    Swap(ref arr, i, j);
                    i++;
                }
            }
            Swap(ref arr, p, i - 1);
            return i - 1;
        }

        private void Swap(ref int[] arr, int first, int second)
        {
            var tmp = arr[first];
            arr[first] = arr[second];
            arr[second] = tmp;
        }
    }
}
