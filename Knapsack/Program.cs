﻿int[] items = new int[] { 1, 4, 3, 7, 12 };
int size = 21;
Knapsack(items, size);

void Knapsack(int[] items, int size) {
    Array.Sort(items, (a, b) => b.CompareTo(a));
    int max = 0;
    KnapsackRecursive(items, size, 0, 0, ref max);
    Console.WriteLine(max);
}

void KnapsackRecursive(int[] items, int size, int currSize, int currIndex, ref int max) {
    if (currSize <= size) {
        max = Math.Max(max, currSize);
        if (currIndex < items.Length) {
            KnapsackRecursive(items, size, currSize, currIndex + 1, ref max);
            KnapsackRecursive(items, size, currSize + items[currIndex], currIndex + 1, ref max);
        }
    }

}
