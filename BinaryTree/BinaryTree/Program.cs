﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            var nodesArray = 
                File.ReadAllText("data/input.txt")
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            var binaryTree = BinaryTree.FromArray(nodesArray);

            var binarySearchTree = BinarySearchTree.FromBinaryTree(binaryTree);

            var leaves = new List<int>();
            binarySearchTree.TraverseWithActions(
                node => {
                    if (node.Left == null && node.Right == null)
                    {
                        leaves.Add(node.Value);
                    }
                }, 
                null,
                null);

            Console.WriteLine(binarySearchTree.Contains(402));
            Console.WriteLine(binarySearchTree.Contains(1024));

            CalcSum(binarySearchTree, 1940);

            Console.ReadLine();
        }

        private static void CalcSum(BinarySearchTree bst, int sum)
        {
            int endPathNodeValue;
            bst.TraverseDFSInOrder(
                node =>
                {
                    endPathNodeValue = CalcSumRecursive(node, 0, sum);
                    if (endPathNodeValue != 0)
                    {
                        Console.WriteLine(bst.GetPath(endPathNodeValue));
                    }
                });
        }

        private static int CalcSumRecursive(BinaryTreeNode node, int currSum, int sum)
        {
            if (node != null)
            {   
                currSum += node.Value;
                if (currSum == sum)
                {
                    return node.Value;
                }

                if (currSum < sum)
                {
                    var res = CalcSumRecursive(node.Left, currSum, sum);
                    if (res != 0)
                    {
                        return res;
                    }
                    res = CalcSumRecursive(node.Right, currSum, sum);
                    if (res != 0)
                    {
                        return res;
                    }
                }
                
            }
            return 0;
        }

        private class TreePath
        {

        }
    }
}
