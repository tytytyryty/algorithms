﻿

Console.WriteLine(ReorganizeString("aab")); 

/*
Input: s = "aab"
Output: "aba"

Input: s = "aaab"
Output: ""

Input: aaabc 
Output: abaca 

Input: bcaaa 
Output: abaca 

a a
0 1
1 2
*/

string ReorganizeString(string s) {
    var charFrequencies = new Dictionary<char, int>();
    foreach (var c in s)
    {
        if (charFrequencies.ContainsKey(c))
            charFrequencies[c]++;
        else 
            charFrequencies.Add(c, 1);
    }

    var p = new PriorityQueue<Key, int>(Comparer<int>.Create((a,b)=>b.CompareTo(a)));
    foreach (var charFrequency in charFrequencies)
    {
        p.Enqueue(new Key { character = charFrequency.Key, frequency = charFrequency.Value }, charFrequency.Value);
    }

    var newS = "";
    var prev = new Key {frequency = 0};

    while (p.Count > 0)
    {
        var i = p.Dequeue();
        newS += i.character;
        i.frequency --;

        if (prev.frequency > 0) {
            p.Enqueue(prev, prev.frequency);
        }
        prev = i;
    }
    if (s.Length != newS.Length) {
        return "";
    }
    
    return newS;
}

struct Key {
    public char character;
    public int frequency;
}


