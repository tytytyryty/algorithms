﻿
using System;

namespace HashTable.HashFunctions
{
    class DoubleHashingFunction : IProbingHashFunction
    {   
        public long Hash(long key, long m, long i)
        {
            var k = Math.Abs(key);
            var h1 = k % m;
            var h2 = i * (1 + k % (m - 1));
            return (h1 + h2) % m;
        }
    }
}
