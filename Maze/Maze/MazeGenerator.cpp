#include "MazeGenerator.h"

MazeGenerator::MazeGenerator()
{
}


MazeGenerator::~MazeGenerator()
{
}


Maze MazeGenerator::Generate(int height, int width, bool onePath) 
{
	int rows = height;
	int cols = width;

	//Point* enter = &Point(0, 0);
	//Point* exit = &Point(9, 9);
	
	//Maze maze(rows, cols, enter, exit);
	Maze maze(rows, cols);

	Point entryPoint(0, 0);

	stack<Point> pointsStack;
	pointsStack.push(entryPoint);

	srand(time(NULL));
	while (!pointsStack.empty())
	{
		Point currentPoint = pointsStack.top();

		pair<NeighbourCell*, int> neighboursToGoResult = getNeighboursToGo(currentPoint, maze);
		NeighbourCell* neighboursToGo = neighboursToGoResult.first;
		int neighboursToGoCount = neighboursToGoResult.second;

		if (neighboursToGoCount > 0)
		{
			int choosedNeightbourIndex = rand() % neighboursToGoCount;
			NeighbourCell choosedNeighbour = neighboursToGo[choosedNeightbourIndex];
			
			//why can do here?
			delete[] neighboursToGo;
			
			if (choosedNeighbour.direction == Direction::top)
			{
				maze.index(currentPoint.row, currentPoint.col).hasTopWall = false;
				maze.index(choosedNeighbour.point.row, choosedNeighbour.point.col).hasBottomWall = false;
			}
			if (choosedNeighbour.direction == Direction::bottom)
			{
				maze.index(currentPoint.row, currentPoint.col).hasBottomWall = false;
				maze.index(choosedNeighbour.point.row, choosedNeighbour.point.col).hasTopWall = false;
			}
			if (choosedNeighbour.direction == Direction::left)
			{
				maze.index(currentPoint.row, currentPoint.col).hasLeftWall = false;
				maze.index(choosedNeighbour.point.row, choosedNeighbour.point.col).hasRightWall = false;
			}
			if (choosedNeighbour.direction == Direction::right)
			{
				maze.index(currentPoint.row, currentPoint.col).hasRightWall = false;
				maze.index(choosedNeighbour.point.row, choosedNeighbour.point.col).hasLeftWall = false;
			}

			(*interimGenerationStep)(maze);

			pointsStack.push(choosedNeighbour.point);
		}
		else 
		{
			pointsStack.pop();
		}
	}
	
	return maze;
}

void MazeGenerator::OnInterimGenerationStep(void (*interimGenerationStep)(Maze maze)) {
	this->interimGenerationStep = interimGenerationStep;
}

pair<NeighbourCell*, int> MazeGenerator::getNeighboursToGo(Point const& point, Maze &maze)
{
	//point.col = 5;
	//(*point).col = 5;
	
	NeighbourCell* neighboursToGo = new NeighbourCell[CELL_NEIGHBOURS_COUNT];
	int neighboursToGoCount = 0;

	MazeCell currentCell = maze.index(point.row, point.col);

	Point topPoint    = Point(point.row - 1, point.col);
	Point bottomPoint = Point(point.row + 1, point.col);
	Point leftPoint   = Point(point.row, point.col - 1);
	Point rightPoint  = Point(point.row, point.col + 1);
	
	if (topPoint.row >= 0 && maze(topPoint.row, topPoint.col).hasAllWalls())
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(topPoint, Direction::top);
		neighboursToGoCount++;
	}
	if (bottomPoint.row < maze.rows && maze(bottomPoint.row, bottomPoint.col).hasAllWalls())
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(bottomPoint, Direction::bottom);
		neighboursToGoCount++;
	}
	if (leftPoint.col >= 0 && maze(leftPoint.row, leftPoint.col).hasAllWalls())
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(leftPoint, Direction::left);
		neighboursToGoCount++;
	}
	if (rightPoint.col < maze.cols && maze(rightPoint.row, rightPoint.col).hasAllWalls())
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(rightPoint, Direction::right);
		neighboursToGoCount++;
	}
	
	return pair<NeighbourCell*, int>(neighboursToGo, neighboursToGoCount);
}