public class Dijkstra {
    //O(V^2) 
    public static void FindShortestPathsToAll(int[,] graph, int source) {
        var numVertices = graph.GetLength(0);

        var distances = new int[numVertices];
        initDistancesToMax(distances, source);
        
    }

    private static void initDistancesToMax(int[] distances, int source){
        for (int i = 0; i < distances.Length; i++) {
            if (i == source) {
                distances[i] = 0;
            } else {
                distances[i] = int.MaxValue;
            }
        }
    }
}