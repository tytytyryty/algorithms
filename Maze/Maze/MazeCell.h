#pragma once
struct MazeCell
{
	MazeCell()
	{
		
	}

	bool hasTopWall = true; 
	bool hasBottomWall = true;
	bool hasLeftWall = true;
	bool hasRightWall = true; 
	
	bool isEnter = false;
	bool isExit = false;

	bool hasPlayer = false;
	bool hasPath = false;

	bool isVisited = false;

	bool hasAllWalls() 
	{
		return hasTopWall == true && hasBottomWall == true && hasLeftWall == true && hasRightWall == true;
	}
};