﻿using System.Collections.ObjectModel;
using System.Linq;

namespace Solution
{
    using OrderMatchingEngine;
    class Solution
    {
        static void Main(string[] args)
        {
            var orderMatchingEngine = new OrderMatchingEngine();

            string inputLine;
            do
            {
                inputLine = Console.ReadLine();
                if (inputLine == "END")
                {
                    break;
                }

                var splittedInputLine = inputLine.Split(" ");
                switch (splittedInputLine[0])
                {
                    case "SUB":
                        var order = new Order(
                            type: orderTypesMap[splittedInputLine[1]],
                            isBuy: splittedInputLine[2] == "B" ? true : false,
                            id: splittedInputLine[3],
                            quantity: int.Parse(splittedInputLine[4]),
                            price: splittedInputLine.ElementAtOrDefault(5) == null ? 0 : int.Parse(splittedInputLine[5]),
                            displaySize: splittedInputLine.ElementAtOrDefault(5) == null ? 0 : int.Parse(splittedInputLine[6]));
                        var trades = orderMatchingEngine.ProcessOrder(order);
                        printTradesTotalPrice(trades);
                        break;
                    case "CXL":
                            orderMatchingEngine.CancelOrder(id: splittedInputLine[1]);
                        break;
                        case "CRP":
                            orderMatchingEngine.CancelReplaceLimitOrder(
                                id: splittedInputLine[1], 
                                newQuantity: int.Parse(splittedInputLine[2]),
                                newPrice: int.Parse(splittedInputLine[3]));
                        break;
                }
            } while (true);

            printOrderBook(
                buyOrders: orderMatchingEngine.GetBuyOrdersFromOrderBook(),
                sellOrders: orderMatchingEngine.GetSellOrdersFromOrderBook()
            );
        }

        private static Dictionary<string, OrderType> orderTypesMap = new Dictionary<string, OrderType>() {
                {"LO", OrderType.Limit},
                {"MO", OrderType.Market},
                {"IOC", OrderType.ImmediateOrCancel},
                {"FOK", OrderType.FillOrKill},
                {"ICE", OrderType.Iceberg},
        };

        private static void printTradesTotalPrice(ReadOnlyCollection<Trade> trades)
        {
            var totalPrice = 0;
            foreach (var trade in trades)
            {
                totalPrice += trade.Quantity * trade.Price;
            }
            Console.WriteLine(totalPrice);
        }

        private static void printOrderBook(ReadOnlyCollection<Order> buyOrders, ReadOnlyCollection<Order> sellOrders)
        {
            Console.Write("B: ");
            printOrders(buyOrders);
            Console.WriteLine();

            Console.Write("S: ");
            printOrders(sellOrders);
            Console.WriteLine();
        }

        private static void printOrders(ReadOnlyCollection<Order> orders)
        {
            foreach (Order order in orders)
            {
                Console.Write($"{order.Quantity}@{order.Price}#{order.Id} ");
            }
        }
    }
}

namespace OrderMatchingEngine
{
    class OrderMatchingEngine
    {
        private SortedSet<Order> buyOrders = new SortedSet<Order>(new ByHighestPriceAndEarliestTimestampComparer());
        private SortedSet<Order> sellOrders = new SortedSet<Order>(new ByLowestPriceAndEarliestTimestampComparer());

        public ReadOnlyCollection<Trade> ProcessOrder(Order order)
        {
            if (order.Type != OrderType.Iceberg) {
                return processOrder(order);
            } else {
                var allTrades = new List<Trade>();
                ReadOnlyCollection<Trade> currentTrades = null;
                do {
                    currentTrades = processOrder(order);
                    allTrades.AddRange(currentTrades);
                } while (currentTrades != null || currentTrades.Count() > 0);
                return allTrades.AsReadOnly();
            }
        }

        private ReadOnlyCollection<Trade> processOrder(Order order)
        {
            var trades = new List<Trade>();

            var ordersOfCurrentType = order.IsBuy ? buyOrders : sellOrders;
            var ordersToMatch = order.IsBuy ? sellOrders : buyOrders;
            var fulfilledOrdersToMatch = new List<Order>();
            var icebergOrdersToRefill = new List<Order>();

            if (ordersToMatch.Count == 0)
            {
                if (order.Type == OrderType.Limit)
                {
                    ordersOfCurrentType.Add(order);
                }
                return trades.AsReadOnly();
            }

            if (order.Type == OrderType.FillOrKill)
            {
                if (ordersToMatch
                    .Where(o => (order.IsBuy && order.Price >= o.Price) || (!order.IsBuy && order.Price <= o.Price))
                    .Select(o => o.Quantity).Sum() <= order.Quantity)
                {
                    return trades.AsReadOnly();
                }
            }

            foreach (var orderToMatch in ordersToMatch)
            {
                if (order.Type == OrderType.Limit || order.Type == OrderType.ImmediateOrCancel)
                {
                    if (order.IsBuy && order.Price < orderToMatch.Price ||
                        !order.IsBuy && order.Price > orderToMatch.Price)
                    {
                        if (order.Type == OrderType.Limit)
                        {
                            ordersOfCurrentType.Add(order);
                        }
                        break;
                    }
                }

                int orderQuantity = orderToMatch.Quantity >= order.Quantity
                    ? order.Quantity
                    : orderToMatch.Quantity;

                order.Quantity -= orderQuantity;
                orderToMatch.Quantity -= orderQuantity;
                if (order.Type == OrderType.Iceberg)
                {
                    order.TotalQuantity -= orderQuantity;
                    if (order.TotalQuantity > 0)
                    {
                        icebergOrdersToRefill.Add(order);
                    }
                }

                trades.Add(new Trade(
                    takerOrderID: order.Id,
                    makerOrderID: orderToMatch.Id,
                    quantity: orderQuantity,
                    price: orderToMatch.Price
                ));

                if (orderToMatch.Quantity == 0)
                {
                    fulfilledOrdersToMatch.Add(orderToMatch);
                }
                if (order.Quantity == 0)
                {
                    break;
                }
            }

            removeFulfilledOrdersFromOrderBook(ordersToMatch, fulfilledOrdersToMatch);
            refillIcebergOrders(ordersToMatch, icebergOrdersToRefill);

            return trades.AsReadOnly();
        }

        /*
        private bool cannotFulfillBeforeProcessing(Order order, SortedSet<Order> ordersToMatch ordersToMatch) {
            
        }

        private bool cannotFulfillOnProcessing(Order order, Order orderToMatch) {
            
        }
        */

        private void removeFulfilledOrdersFromOrderBook(SortedSet<Order> ordersToMatch, List<Order> fulfilledOrders)
        {
            foreach (var fulfilledOrder in fulfilledOrders)
            {
                ordersToMatch.Remove(fulfilledOrder);
            }
        }

        private void refillIcebergOrders(SortedSet<Order> ordersToMatch, List<Order> icebergOrdersToRefill)
        {
            foreach (var icebergOrderToRefill in icebergOrdersToRefill)
            {
                var newOrder = new Order(
                    type: icebergOrderToRefill.Type,
                    isBuy: icebergOrderToRefill.IsBuy,
                    id: icebergOrderToRefill.Id,
                    quantity: icebergOrderToRefill.TotalQuantity,
                    price: icebergOrderToRefill.Price, 
                    displaySize: icebergOrderToRefill.DisplaySize);
                ordersToMatch.Add(newOrder);
            }
        }

        public void CancelOrder(string id)
        {
            var order = buyOrders.FirstOrDefault(o => o.Id == id);
            if (order != null) {
                buyOrders.Remove(order);
                return;
            } 

            order = sellOrders.FirstOrDefault(o => o.Id == id);
            if (order != null) {
                sellOrders.Remove(order);
            }
        }

        public void CancelReplaceLimitOrder(string id, int newQuantity, int newPrice)
        {
            var order = buyOrders.FirstOrDefault(o => o.Id == id);
            if (order == null) {
                order = sellOrders.FirstOrDefault(o => o.Id == id);
                if (order == null || order.Type != OrderType.Limit) {
                    return;
                }
            }

            if (newPrice == order.Price && newQuantity <= order.Quantity) {
                order.Quantity = newQuantity;
            } else {
                var newOrder = new Order(
                    type: order.Type,
                    isBuy: order.IsBuy,
                    id: order.Id,
                    quantity: newQuantity,
                    price: newPrice);
                
                if (order.IsBuy) {
                    buyOrders.Remove(order);
                    buyOrders.Add(newOrder);
                } else {
                    sellOrders.Remove(order);
                    sellOrders.Add(newOrder);
                }
            }
        }

        public ReadOnlyCollection<Order> GetBuyOrdersFromOrderBook()
        {
            return buyOrders.ToList().AsReadOnly();
        }

        public ReadOnlyCollection<Order> GetSellOrdersFromOrderBook()
        {
            return sellOrders.ToList().AsReadOnly();
        }

        private class ByHighestPriceAndEarliestTimestampComparer : IComparer<Order>
        {
            public int Compare(Order order1, Order order2)
            {
                var result = order2.Price.CompareTo(order1.Price);
                if (result == 0)
                {
                    result = order1.Timestamp.CompareTo(order2.Timestamp);
                }
                return result;
            }
        }

        private class ByLowestPriceAndEarliestTimestampComparer : IComparer<Order>
        {
            public int Compare(Order order1, Order order2)
            {
                var result = order1.Price.CompareTo(order2.Price);
                if (result == 0)
                {
                    result = order1.Timestamp.CompareTo(order2.Timestamp);
                }
                return result;
            }
        }
    }

    class Order
    {
        public Order(OrderType type, bool isBuy, string id, int quantity, int price = 0, int displaySize = 0)
        {

            Type = type;
            IsBuy = isBuy;
            Id = id;
            Quantity = type == OrderType.Iceberg ? displaySize : quantity;
            Price = price;
            DisplaySize = displaySize;
            TotalQuantity = quantity;
            Timestamp = DateTime.Now;
        }
        public string Id { get; }
        public OrderType Type { get; }
        public bool IsBuy { get; }
        public int Quantity { get; set; }
        public int Price { get; }
        public int DisplaySize { get; set; }
        public int TotalQuantity { get; set; }
        public DateTime Timestamp { get; }
    }

    public enum OrderType
    {
        Limit,
        Market,
        ImmediateOrCancel, 
        FillOrKill,
        Iceberg
    }

    class Trade
    {
        public Trade(string takerOrderID, string makerOrderID, int quantity, int price)
        {
            TakerOrderID = takerOrderID;
            MakerOrderID = makerOrderID;
            Quantity = quantity;
            Price = price;
        }
        public string TakerOrderID { get; }
        public string MakerOrderID { get; }
        public int Quantity { get; }
        public int Price { get; }
    }
}
