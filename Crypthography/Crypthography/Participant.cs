﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypthography
{
    public class Participant
    {
        private string _name;
        private int _privateKey;
        private int p = PublicNumbers.p;
        private int g = PublicNumbers.g;

        public Participant(string name, int privateKey)
        {
            _name = name;
            _privateKey = privateKey;
        }

        public double PublicKey
        {
            get { return Math.Pow(g, _privateKey) % p; }
        }
         
        //Diffy Hellman algorithm of common secret key creation without sending private key
        public double GetCommonSecretKeyWith(Participant other)
        {
            return Math.Pow(other.PublicKey, _privateKey) % p; 
        }
    }
}
