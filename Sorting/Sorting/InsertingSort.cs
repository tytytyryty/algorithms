﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    class InsertingSort : ISortable
    {
        public IEnumerable<int> Sort(IEnumerable<int> array)
        {
            var arrayToSort = array.ToArray();
            //на протяжении всего цикла соблюдается инвариантность массива
            //начинаем со второй карты колоды
            for (int j = 1; j < arrayToSort.Length; j++)
            {
                //запоминаем карту которую будем сдвигать влево
                var key = arrayToSort[j];
                //начиная с карты левее и далее пока будут карты больше текущей  
                int i = j - 1;
                while (i > -1 && arrayToSort[i] > key)
                {
                    //вставляем карту на одну позицию вправо 
                    arrayToSort[i + 1] = arrayToSort[i];
                    i--;
                }
                //вставляем карту которую запоминаем на позицию последней сдвинутой карты
                arrayToSort[i + 1] = key;
            }
            return arrayToSort;
        }
    }
}
