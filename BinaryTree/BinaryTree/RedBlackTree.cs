﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class RedBlackTree : BinarySearchTree
    {
        public static RedBlackTree FromBinaryTree(BinaryTree binaryTree)
        {
            var bst = BinarySearchTree.FromBinaryTree(binaryTree);
            var rbt = Balance(bst);
            return rbt;
        }

        private static RedBlackTree Balance(BinarySearchTree bst)
        {
            return null;
        }
    }
}
