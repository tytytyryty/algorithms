﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArrayInversions
{
    class Program
    {
        public static void Main(string[] args)
        {
            Quicksort sort = new Quicksort();

            sort.Sort();

            Console.WriteLine(sort);
            Console.WriteLine(sort.Counter);

            Console.ReadKey(true);
        }
    }


    class Quicksort
    {
        public int Counter = 0;
        private int[] data;
        private Random rng = new Random();

        public Quicksort()
        {
            //UInt64 count = 0;
            //var before = DateTime.Now.Ticks;
            //var arr = array.ToArray();
            //foreach (var a in arr)
            //{
            //    Console.Write(a + " ");
            //}
            //count = _counter;
            //var after = DateTime.Now.Ticks;
            //Console.WriteLine(count);
            //Console.WriteLine(new TimeSpan(after - before).TotalSeconds);
            //Console.ReadLine();

            List<int> array = new List<int>() { 1, 3, 5, 2, 4, 6 };
            string[] strArray = File.ReadAllLines("IntegerArray.txt");
            array.Clear();
            foreach (var str in strArray)
            {
                array.Add(int.Parse(str));
            }

            data = new int[strArray.Length];
            //Test Data from TextBook:
            data = array.ToArray();


        } // end constructor

        public void Sort()
        {
            // Calls the 'private' recursive Quicksort method
            recSort(0, data.Length - 1);
        }

        private void recSort(int left, int right)
        {
            // Recursively calls itself until the pointers collide
            // And as the partitioning works in-place there's no
            // writing at the end, the array is already sorted
            if (left < right)
            {
                int pivot = Partition(left, right);
                recSort(left, pivot-1);
                recSort(pivot+1, right);
            }

        }

        private int Partition(int left, int right)
        {
            //int pivotIndex = left;
            //Swap(left, right);

            int swapIndex = 0;
            int median = (right + left)/2;
            var a = new int[] {data[left], data[right], data[median]};
            var sordedA = a.OrderBy(i => i).ToArray();
            if (sordedA[1] == data[left])
            {
                swapIndex = left;
            }
            if (sordedA[1] == data[right])
            {
                swapIndex = right;
            }
            if (sordedA[1] == data[median])
            {
                swapIndex = median;
            }
            Swap(left, swapIndex);

            int pivotIndex = left;
            int j = left+1;
            int pivot = data[pivotIndex];
            for (int i = left+1; i <= right; i++)
            {
                Counter++;
                if (data[i] < pivot)
                {
                    Swap(i, j);
                    j++;
                }
            }

            Swap(pivotIndex, j-1);

            return j-1;
        }

        private void Swap(int i, int j)
        {
            int temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }

        public override string ToString()
        {
            // Display a pretty version of the array
            // Make sure SB doesn't have to increase capacity as it appends
            StringBuilder temp = new StringBuilder((data.Length * 6) + 1);

            for (int i = 0; i < data.Length; i++)
                temp.AppendFormat("{0} ", data[i]);

            return temp.ToString();
        }

    } // end class
}
