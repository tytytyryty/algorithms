﻿//var implementation = Implementation.Iterative;
//var implementation = Implementation.Recursive;
//var implementation = Implementation.Tabulation;
var implementation = Implementation.Memoization;

Console.WriteLine(Fibonacci(1, implementation));
Console.WriteLine(Fibonacci(2, implementation));
Console.WriteLine(Fibonacci(3, implementation));
Console.WriteLine(Fibonacci(4, implementation));
Console.WriteLine(Fibonacci(5, implementation));
Console.WriteLine(Fibonacci(6, implementation));
Console.WriteLine(Fibonacci(7, implementation));

static int Fibonacci(int n, Implementation implementation) {
    switch (implementation) {
        case Implementation.Iterative: return FibonacciIterative(n);
        case Implementation.Recursive: return FibonaccinRecursive(n);
        case Implementation.Tabulation: return FibonacciTabulation(n);
        case Implementation.Memoization: return FibonacciMemoization(n, new Dictionary<int, int>());
        default: return -1;
    }
}

static int FibonacciIterative(int n) {
    if (n == 1 || n == 2) {
        return 1;
    }
    var prev1 = 1;
    var prev2 = 1;
    for (int i = 3; i <= n; i++) {
        var curr = prev1 + prev2;
        prev1 = prev2;
        prev2 = curr;
    }
    return prev2;
}

static int FibonaccinRecursive(int n) {
    if (n == 1 || n == 2) {
        return 1;
    } else {
        return FibonaccinRecursive(n-1) + FibonaccinRecursive(n-2);
    }
}

//bottom-up approach
static int FibonacciTabulation(int n) {
    if (n == 1 || n == 2) {
        return 1;
    } else {
        var tab = new int[n+1];
        tab[1] = 1;
        tab[2] = 1;
        for(int i = 3; i <= n; i++) {
            tab[i] = tab[i-1] + tab[i-2];
        }
        return tab[n];
    }
}

//top-down approach
static int FibonacciMemoization(int n, Dictionary<int, int> cache) {
    if (cache.ContainsKey(n)) {
        return cache[n];
    }
    int result;
    if (n == 1 || n == 2) {
        return 1;
    } else {
        result = FibonacciMemoization(n-1, cache) + FibonacciMemoization(n-2, cache);
    }
    cache[n] = result;
    return result;
}



enum Implementation {
    Iterative,
    Recursive,
    Tabulation,
    Memoization
}