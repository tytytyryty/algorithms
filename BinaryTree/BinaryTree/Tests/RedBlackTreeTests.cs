﻿
using NUnit.Framework;

namespace BinaryTree
{
    [TestFixture]
    class RedBlackTreeTests
    {
        [Test]
        public void FromBinaryTreeWorksCorrectly()
        {
            //arrange
            var initialBinaryTree = new BinaryTree
            {
                Root = new BinaryTreeNode(
                        1,
                        new Left(4,
                            new Left(6,
                                new Left(10)),
                            new Right(7,
                                new Right(8))
                            ),
                        new Right(2,
                            new Left(5),
                            new Right(3,
                                new Left(9))
                            )
                        )
            };
            
            //act
            var rbt = RedBlackTree.FromBinaryTree(initialBinaryTree);

            //assert
            Assert.IsTrue(CheckRedBlackTreeRules(rbt));
        }
        
        private bool CheckRedBlackTreeRules(RedBlackTree rbt)
        {
            return 
                RootIsBlack(rbt) && 
                AllNillLeavesAreBlack(rbt) && 
                BothChildsOfRedAreBlack(rbt) && 
                AllPathesToLeavesContainSameNumberOfBlackNodes(rbt);
        }

        #region red-black tree rules
        
        public bool RootIsBlack(RedBlackTree rbt)
        {
            return rbt.Root.Color == NodeColor.Black;
        }
        
        public bool AllNillLeavesAreBlack(RedBlackTree rbt)
        {
            return rbt.Root.Color == NodeColor.Black;
        }
        
        public bool BothChildsOfRedAreBlack(RedBlackTree rbt)
        {
            var result = true;

            rbt.TraverseWithActions(
                node => {
                    if (node.Color == NodeColor.Red)
                    {
                        if (node.Left.Color == NodeColor.Red || node.Right.Color == NodeColor.Red)
                        {
                            result = false;
                        }
                    }
                },
                null, 
                null);

            return result;
        }
        
        public bool AllPathesToLeavesContainSameNumberOfBlackNodes(RedBlackTree rbt)
        {
            var result = true;
            var blackHeightLeft = 0;
            var blackHeightRight = 0;

            rbt.TraverseWithActions(
                node => {
                    if (node.Color == NodeColor.Black)
                    {
                        blackHeightLeft++;
                    }
                },
                node => {
                    if (node.Color == NodeColor.Black)
                    {
                        blackHeightRight++;
                    }
                },
                node => {
                    if (blackHeightLeft != blackHeightRight)
                    {
                        result = false;
                    }
                });

            return result;
        }

        #endregion
    }
}
