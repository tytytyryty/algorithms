﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karatsuba
{
    class Program
    {
        static void Main(string[] args)
        {
            //6578
            //1234
            //8117252
            //var result = new KaratsubaMultiplication().Multiply("1685287499328328297814655639278583667919355849391453456921116729", "7114192848577754587969744626558571536728983167954552999895348492");
            //var result = new KaratsubaMultiplication().Multiply("11", "3");
            var result = new KaratsubaMultiplication().Multiply("1234", "5678");
            System.IO.File.WriteAllText("r.txr", result);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
