﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] array = new int[] {1, 10, 122, 15, 4, 22, 141, 2, 76, 12, 55, 135, 11, 7};
            //int[] array = { 2, 5, 13, 8, 9, 17 };

            var array = File.ReadAllText("QuickSort.txt").Split(new[] { "\n" }, StringSplitOptions.None).Select(int.Parse);

            var sortedArray = new QuickSort().Sort(array);
            Output(sortedArray);
        }

        static void Output(IEnumerable<int> arrayToOutput)
        {
            foreach (var i in arrayToOutput)
            {
                Console.Write(i + " ");
            }
            Console.ReadLine();
        }
    }
}
