﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypthography
{
    class Program
    {
        static void Main(string[] args)
        {
            var alice = new Participant("Alice", 6);
            var bob = new Participant("Bob", 15);
            
            Console.WriteLine("Alice's public key {0}", alice.PublicKey);
            Console.WriteLine("Bob's public key {0}", bob.PublicKey);

            Console.WriteLine(alice.GetCommonSecretKeyWith(bob));
            Console.WriteLine(bob.GetCommonSecretKeyWith(alice));

            Console.ReadLine();
        }
    }
}
