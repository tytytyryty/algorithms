﻿
namespace HashTable.HashFunctions
{
    public interface IProbingHashFunction
    {
        long Hash(long key, long m, long i);
    }
}
