#include "Maze.h"
#include <iostream>

Maze::Maze(int rows, int cols, Point* enterPointPtr, Point* exitPointPtr)
{
	this->rows = rows;
	this->cols = cols;
	
	size = this->rows * this->cols;
	maze = unique_ptr<MazeCell[]>(new MazeCell[size]);

	for (int i = 0; i < size; i++)
	{
		maze[i] = MazeCell();
	}

	setMazeEnter(enterPointPtr);
	setMazeExit(exitPointPtr);
}

Maze::~Maze()
{
	//delete[] maze;
}

MazeCell& Maze::index(int i, int j) const
{
	return maze[i * cols + j];
}

MazeCell& Maze::operator()(int i, int j) const
{
	return index(i, j);
}

MazeCell& Maze::index(Point point) const
{
	return index(point.row, point.col);
}

MazeCell& Maze::operator()(Point point) const
{
	return index(point.row, point.col);
}

void Maze::setMazeEnter(Point* enterPointPtr)
{
	Point enterPoint;

	if (enterPointPtr != nullptr)
	{
		enterPoint = Point((*enterPointPtr).row, (*enterPointPtr).col);
	}
	else
	{
		enterPoint = Point(0, 0);
	}

	index(enterPoint.row, enterPoint.col).isEnter = true;
	Maze::enterPoint = enterPoint;
}

void Maze::setMazeExit(Point* exitPointPtr)
{
	Point exitPoint;
	if (exitPointPtr != nullptr)
	{
		exitPoint = Point(exitPointPtr->row, exitPointPtr->col);
	}
	else
	{
		exitPoint = Point(rows - 1, cols - 1);
	}

	index(exitPoint.row, exitPoint.col).isExit = true;
	Maze::exitPoint = exitPoint;
}