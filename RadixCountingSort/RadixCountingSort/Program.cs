﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixCountingSort
{
    class Program
    {
        const int distinctSymbolsCount = 26; //english alphabet abcdefghijklmnopqrstuvwxyz 

        static void Main(string[] args)
        {
            var strings = File.ReadAllLines("input.txt");

            var resultStrings = RadixSort(strings);

            var letter = MaxFreequencyLetter(strings);
        }

        static string[] RadixSort(string[] array)
        {
            for (int i = 2; i >= 0; i--)
            {
                array = CountingSort(array, i);
            }

            return array;
        }
        
        static string[] CountingSort(string[] arrToSort, int digit)
        {
            var auxiliaryArr = new int[distinctSymbolsCount];
            foreach (var c in arrToSort)
            {
                var pos = GetCharNumber(c[digit]);
                auxiliaryArr[pos]++;
            }
            
            for (int i = 1; i < distinctSymbolsCount; i++)
            {
                auxiliaryArr[i] += auxiliaryArr[i-1];
            }

            var resultArray = new string[arrToSort.Length];
            for (int i = arrToSort.Length - 1; i >= 0 ; i--)
            {
                var initial = arrToSort[i];
                var pos = GetCharNumber(initial[digit]);
                resultArray[auxiliaryArr[pos] - 1] = initial;
                auxiliaryArr[pos]--;
            }

            return resultArray;
        }

        static int GetCharNumber(char c)
        {
            switch (c)
            {   
                case 'a': return 0;
                case 'b': return 1;
                case 'c': return 2;
                case 'd': return 3;
                case 'e': return 4;
                case 'f': return 5;
                case 'g': return 6;
                case 'h': return 7;
                case 'i': return 8;
                case 'j': return 9;
                case 'k': return 10;
                case 'l': return 11;
                case 'm': return 12;
                case 'n': return 13;
                case 'o': return 14;
                case 'p': return 15;
                case 'q': return 16;
                case 'r': return 17;
                case 's': return 18;
                case 't': return 19;
                case 'u': return 20;
                case 'v': return 21;
                case 'w': return 22;
                case 'x': return 23;
                case 'y': return 24;
                case 'z': return 25;
                default: return -1;
            }
        }

        static int MaxFreequencyLetter(string[] strings)
        {
            var auxiliaryArr = new int[26];

            foreach (var s in strings)
            {
                var pos1 = GetCharNumber(s[0]);
                var pos2 = GetCharNumber(s[1]);
                var pos3 = GetCharNumber(s[2]);
                auxiliaryArr[pos1]++;
                auxiliaryArr[pos2]++;
                auxiliaryArr[pos3]++;
            }

            int maxValue = auxiliaryArr.Max();
            int maxIndex = Array.IndexOf(auxiliaryArr, maxValue);

            return maxIndex;
        }
    }
}
