﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace TSP
{
    public class TravellingSalesmanProblem
    {
        private string fileName;
        private List<int> currentOrder = new List<int>();
        private List<int> nextOrder = new List<int>();
        private double[,] distances;
        private Random random = new Random();
        private double shortestDistance = 0;
        private int currentNeighbor = 0;

        public double ShortestDistance
        {
            get
            {
                return shortestDistance;
            }
            set
            {
                shortestDistance = value;
            }
        }

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public List<int> CitiesOrder
        {
            get
            {
                return currentOrder;
            }
            set
            {
                currentOrder = value;
            }
        }

        private void LoadCities(int startSolveId)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FileName+".xml");
            XmlNode graph = doc.SelectSingleNode("travellingSalesmanProblemInstance/graph");
            XmlNodeList vertexList = graph.SelectNodes("vertex");
            distances = new double[vertexList.Count, vertexList.Count];
            for (int vertexId = 0; vertexId < vertexList.Count; vertexId++)
            {
                XmlNodeList edgeList = vertexList[vertexId].SelectNodes("edge");
                for (int edgeId = 0; edgeId < edgeList.Count; edgeId++)
                {
                    string strDistance = edgeList[edgeId].Attributes["cost"].Value.Replace('.', ',');
                    double distance = double.Parse(strDistance.Substring(0, 17)) * Math.Pow(10, double.Parse(strDistance.Substring(19, 2)));
                    distances[vertexId, edgeId] = distance;
                }
            }
            string solveLine = File.ReadAllLines(fileName + ".txt")[startSolveId];
            string separator = " - ";
            if (solveLine.Contains("->"))
            {
                separator = " -> ";
            }

            string[] startSolve = solveLine.Split(new string[] { separator }, new StringSplitOptions());
            for (int i = 0; i < startSolve.Count(); i++)
            {
                currentOrder.Add(Convert.ToInt32(startSolve[i]));
            }

            for (int i = 0; i < vertexList.Count; i++)
            {
                for (int j = 0; j < vertexList.Count; j++)
                {
                    if (distances[i, j] < 0.0000000001)
                    {
                        distances[i, j] = 0.0000000001;
                    }
                }
            }
        }

        private double GetTotalDistance(List<int> order)
        {
            double distance = 0;

            for (int i = 0; i < order.Count - 1; i++)
            {
                distance += distances[order[i], order[i + 1]];
            }

            if (order.Count > 0)
            {
                distance += distances[order[order.Count - 1], 0];
            }

            return distance;
        }

        private List<int> GetNextArrangementRandom(List<int> order)
        {
            List<int> newOrder = new List<int>();

            for (int i = 0; i < order.Count; i++)
            {
                newOrder.Add(order[i]);
            }

            int firstRandomCityIndex = random.Next(1, newOrder.Count);
            int secondRandomCityIndex = random.Next(1, newOrder.Count);

            int dummy = newOrder[firstRandomCityIndex];
            newOrder[firstRandomCityIndex] = newOrder[secondRandomCityIndex];
            newOrder[secondRandomCityIndex] = dummy;

            return newOrder;
        }

        private List<int> GetNextArrangementNeighbor(List<int> order)
        {
            List<int> newOrder = new List<int>();
            for (int i = 0; i < order.Count; i++)
            {
                newOrder.Add(order[i]);
            }

            currentNeighbor++;
            if (currentNeighbor < order.Count - 1)
            {
                
                var tmp = newOrder[currentNeighbor];
                newOrder[currentNeighbor] = newOrder[currentNeighbor + 1];
                newOrder[currentNeighbor + 1] = tmp;

            }
            return newOrder;
        }

        public void Anneal(int startSolveId)
        {
            int iteration = 0;

            double temperature = 1000000.0;
            double deltaDistance = 0;
            double coolingRate = 0.99999;
            double absoluteTemperature = 0.00001;

            LoadCities(startSolveId);

            double distance = GetTotalDistance(currentOrder);
            double recDistance = distance;

            while (temperature >= absoluteTemperature)
            {
                nextOrder = GetNextArrangementRandom(currentOrder);

                deltaDistance = GetTotalDistance(nextOrder) - distance;

                if (Math.Exp(-deltaDistance/temperature) > random.NextDouble())
                {
                    for (int i = 0; i < nextOrder.Count; i++)
                    {
                        currentOrder[i] = nextOrder[i];
                    }

                    distance = deltaDistance + distance;
                    
                    if (distance < recDistance)
                    {
                        recDistance = distance;
                    }

                }
                
                temperature *= coolingRate;
                
                iteration++;
            }
            //Console.WriteLine(iteration);

            shortestDistance = recDistance;
        }
        
        public void LocalSearch(int startSolveId)
        {
            int stealIteration = 0;
            int iteration = 0;

            int maxStealIteration = 10000;

            double deltaDistance = 0;

            LoadCities(startSolveId);

            double distance = GetTotalDistance(currentOrder);

            while (maxStealIteration > stealIteration)
            {
                nextOrder = GetNextArrangementRandom(currentOrder);

                deltaDistance = GetTotalDistance(nextOrder) - distance;

                if (deltaDistance < 0)
                {
                    for (int i = 0; i < nextOrder.Count; i++)
                    {
                        currentOrder[i] = nextOrder[i];
                    }

                    distance = deltaDistance + distance;

                    stealIteration = 0;
                }
                else
                {
                    stealIteration++;
                }

                iteration++;
            }
            shortestDistance = distance;
        }

        public void Ant(int startSolveId)
        {
            LoadCities(startSolveId);
            //currentOrder - начальный путь городов

            double alfa = 0.5;
            double beta = 0.5;
            int stealIterationCount = 5000;
            int antCount = CitiesOrder.Count;
            double pherOutSpeed = 0.9;

            double maxF = double.MinValue;
            double minF = double.MaxValue;

            double minFeromon = 0.0000001;
            double maxFeromon = 100;

            List<int> route = new List<int>();
            
            int iteration = 0;
            int stealIteration = 0;

            double[,] pheromons = new double[CitiesOrder.Count, CitiesOrder.Count];

            for (int i = 0; i < CitiesOrder.Count; i++)
            {
                for (int j = 0; j < CitiesOrder.Count; j++)
                {
                    pheromons[i, j] = minFeromon;
                }
            }

            double[] p = new double[CitiesOrder.Count];

            double recordDistance = GetTotalDistance(CitiesOrder);
            List<int> recordRoute = new List<int>();

            //double distance = GetTotalDistance(currentOrder);

            while (stealIterationCount >= stealIteration)
            {
                int nextCityNumber = 0;
                for (int ant=0; ant<antCount; ant++)
                {
                    //memory.Clear();

                    //TODO:
                    //availableCities.Clear();
                    int currentCity = 0;
                    //memory.Add(0);
                    List<int> availableCities = new List<int>();
                    availableCities.AddRange(CitiesOrder);

                    int cityToStart = CitiesOrder[nextCityNumber];
                    nextCityNumber++;

                    availableCities.Remove(cityToStart);

                    route.Clear();
                    route.Add(cityToStart);

                    while (availableCities.Count > 0)
                    {

                        double sum = 0;
                        foreach (var availableCity in availableCities)
                        {
                            sum += Math.Pow(pheromons[currentCity, availableCity], alfa) * 
                                Math.Pow(1/distances[currentCity, availableCity], beta);
                        }

                        double sumP = 0;
                        foreach (var availableCity in availableCities)
                        {

                            p[availableCity] = sumP + (Math.Pow(pheromons[currentCity, availableCity], alfa) * 
                                Math.Pow(1/distances[currentCity, availableCity], beta)) / sum;

                            sumP = p[availableCity];

                        }

                        double r = new Random().NextDouble();

                        foreach (var availableCity in availableCities)
                        {
                            if (p[availableCity] > r)
                            {
                                availableCities.Remove(availableCity);
                                route.Add(availableCity);

                                currentCity = availableCity;
                                break;
                            }
                        }
                    }

                    double currentDistance = GetTotalDistance(route);

                    for (int i = 0; i < CitiesOrder.Count; i++)
                    {
                        for (int j = 0; j < CitiesOrder.Count; j++)
                        {
                            pheromons[i, j] = pherOutSpeed * pheromons[i, j];
                            if (pheromons[i, j] < minFeromon)
                            {
                                pheromons[i, j] = minFeromon;
                            }

                            if (pheromons[i, j] > maxFeromon)
                            {
                                pheromons[i, j] = maxFeromon;
                            }
                        }
                    }

                    for (int i = 0; i < route.Count - 1; i++)
                    {
                        pheromons[route[i], route[i + 1]] += 1 / currentDistance;
                    }

                    pheromons[route[route.Count - 1], route[0]] += 1 / currentDistance;

                    if (currentDistance < recordDistance)
                    {
                        recordDistance = currentDistance;

                        recordRoute.Clear();
                        recordRoute.AddRange(route);

                        stealIteration = 0;
                    }
                    else
                    {
                        stealIteration++;
                    }

                    if (recordRoute.Count > 0)
                    {
                        for (int i = 0; i < recordRoute.Count - 1; i++)
                        {
                            pheromons[recordRoute[i], recordRoute[i + 1]] += 1 / recordDistance;
                        }

                        pheromons[recordRoute[recordRoute.Count - 1], recordRoute[0]] += 1 / recordDistance;
                    }

                }
                
                if (iteration == 900)
                {
                    
                }

                
                for (int i = 0; i < CitiesOrder.Count; i++)
                {
                    for (int j = 0; j < CitiesOrder.Count; j++)
                    {
                        if (pheromons[i, j] > maxF)
                        {
                            maxF = pheromons[i, j];
                        }
                        if (pheromons[i, j] < minF)
                        {
                            minF = pheromons[i, j];
                        } 
                    }
                }

                iteration++;
            }

            //Console.WriteLine("minF " + minF);
            //Console.WriteLine("maxF " + maxF);

            shortestDistance = recordDistance;
            if (recordRoute.Count != CitiesOrder.Count)
            {
            }
            CitiesOrder.Clear();
            CitiesOrder.AddRange(recordRoute);
        }
    }
}