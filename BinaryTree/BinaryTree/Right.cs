﻿
namespace BinaryTree
{
    class Right : BinaryTreeNode
    {
        public Right(int value) : base(value, null, null) { }

        public Right(int value, Left left) : base(value, left, null) { }

        public Right(int value, Right right) : base(value, null, right) { }

        public Right(int value, Left left, Right right) : base(value, left, right) { }
    }
}
