﻿
using System;

namespace BinaryTree
{
    class BinarySearchTree : BinaryTree
    {
        public BinarySearchTree() : base()
        {
        }

        public BinarySearchTree(int root) : base(root)
        {
        }

        public BinarySearchTree(BinaryTreeNode root) : base(root)
        {
        }

        public bool Contains(int value)
        {
            return ContainsRecursive(this.Root, value);
        }

        private bool ContainsRecursive(BinaryTreeNode node, int value)
        {
            if (node != null)
            {
                if (node.Value == value)
                {
                    return true;
                }

                if (value < node.Value)
                {
                    return ContainsRecursive(node.Left, value);
                }
                else
                {
                    return ContainsRecursive(node.Right, value);
                }
            }

            return false;
        }

        public string GetPath(int value)
        {
            return GetPathRecursive(this.Root, value, "");
        }

        private string GetPathRecursive(BinaryTreeNode node, int value, string path)
        {
            if (node != null)
            {
                path += node.Value + " ";

                if (value == node.Value)
                {
                    return path;
                }

                if (value < node.Value)
                {
                    return GetPathRecursive(node.Left, value, path);
                }
                else
                {
                    return GetPathRecursive(node.Right, value, path);
                }
            }

            return path;
        }

        public int[] GetInSortedOrder()
        {
            return base.TraverseDFSInOrder();
        }

        public static BinarySearchTree FromBinaryTree(BinaryTree binaryTree)
        {
            var bst = new BinarySearchTree(binaryTree.Root).Clone(); 
            var inOrderArr = bst.TraverseDFSInOrder();
            Array.Sort(inOrderArr);

            int i = 0;
            bst.TraverseDFSInOrder(node => { node.Value = inOrderArr[i]; i++; });
            return (BinarySearchTree) bst;
        }

        protected override BinaryTree CreateInstanceForClone(int root)
        {
            return new BinarySearchTree(root);
        }
    }
}
