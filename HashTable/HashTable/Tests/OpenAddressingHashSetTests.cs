﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using HashTable.HashFunctions;

namespace HashTable.Tests
{
    [TestFixture]
    public class OpenAddressingHashSetTests
    {
        [Test]
        public void RemoveWorksCorrectly()
        {
            //arrange
            var hashSet = new OpenAddressingHashSet(10, new TestHashFunction());
            hashSet.Add(5);
            hashSet.Add(15);
            hashSet.Add(25);

            //act
            hashSet.Remove(15);

            //assert
            Assert.AreEqual(hashSet.Contains(25), true);
        }

        private class TestHashFunction : IProbingHashFunction
        {
            public long Hash(long key, long m, long i)
            {
                return Math.Abs(key) % m + i;
            }
        }
    }
}
