﻿
namespace BinaryTree
{
    class RedBlackTreeNode : BinaryTreeNode
    {
        public NodeColor Color;

        public RedBlackTreeNode(int value, Left left, Right right, NodeColor color) 
            : base(value, left, right)
        {
            Color = color;    
        }
    }
}
