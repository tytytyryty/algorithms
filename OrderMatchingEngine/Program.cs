﻿using System.Collections.ObjectModel;
using System.Linq;

namespace OrderMatchingEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            var orderBook = new OrderBook();

            orderBook.OnTrade += (trades) => { Console.WriteLine($"Trades made count: {trades.Count}, sum {trades.Sum(t=>t.Amount)}"); };
            orderBook.OnOrderCancelled += (Order) => { Console.WriteLine("Order cancelled"); };

            // orderBook.PlaceOrder(new Order {Amount = 100, Price = 201.00m, IsBuy = true});
            // orderBook.PlaceOrder(new Order {Amount = 300, Price = 202.50m, IsBuy = true});
            // orderBook.PlaceOrder(new Order {Amount = 50,  Price = 202.75m, IsBuy = true});
            // orderBook.PlaceOrder(new Order {Amount = 100, Price = 203.10m, IsBuy = true});

            orderBook.PlaceOrder(new Order {Amount = 100, Price = 201.00m, IsBuy = false});
            orderBook.PlaceOrder(new Order {Amount = 300, Price = 202.50m, IsBuy = false});
            orderBook.PlaceOrder(new Order {Amount = 50,  Price = 202.75m, IsBuy = false});
            orderBook.PlaceOrder(new Order {Amount = 100, Price = 203.10m, IsBuy = false});

            ////////////////MARKET
            //Trades made count: 4, sum 500
            //var order = new Order {Type = OrderType.Market, Amount = 500, IsBuy = false};
            
            ////////////////LIMIT buy
            //Trades made count: 1, sum 100
            //var order = new Order {Type = OrderType.Limit, Price = 202.00m, Amount = 500, IsBuy = true};
            
            ////////////////LIMIT sell
            //Trades made count: 3, sum 450
            //var order = new Order {Type = OrderType.Limit, Price = 202.00m, Amount = 500, IsBuy = false};

            ////////////////LIMIT buy cannot fulfill
            //Trades made count: 0, sum 0
            //var order = new Order {Type = OrderType.Limit, Price = 200.00m, Amount = 500, IsBuy = true};
            
            ////////////////LIMIT sell cannot fulfill
            //Trades made count: 0, sum 0
            //var order = new Order {Type = OrderType.Limit, Price = 204.00m, Amount = 500, IsBuy = false};

            //orderBook.PlaceOrder(order);
            //orderBook.CancelOrder(order);
            //orderBook.ProcessOrder(order);

            var icebergOrder = new IcebergOrder(orderBook, amount:550, splitFactor:4) {
                Type = OrderType.Limit,
                Price = 204.00m,
                IsBuy = true
            };
            icebergOrder.OnFulfilled += () => Console.WriteLine("Iceberg order fulfilled");
            icebergOrder.StartFulfilling();
            
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (sender, e) => processOrders(orderBook);
            timer.Interval = 5000;
            timer.Enabled = true;
            timer.Start();

            Console.WriteLine("Orders are being processed within intervals. Press any key to quit.");
            Console.ReadLine();
        }

        private static void processOrders(OrderBook orderBook) {
            foreach (var buyOrder in orderBook.GetBuyOrders())
            {
                orderBook.ProcessOrder(buyOrder);
            }

            foreach (var sellOrder in orderBook.GetSellOrders())
            {
                orderBook.ProcessOrder(sellOrder);
            }

            printOrders(orderBook);
        }

        private static void printOrders(OrderBook orderBook) { 
            Console.WriteLine("buy orders");
            foreach(Order i in orderBook.GetBuyOrders())
            {
                Console.WriteLine($"Amount {i.Amount} Price {i.Price}");
            }

            Console.WriteLine("sell orders");
            foreach(Order i in orderBook.GetSellOrders())
            {
                Console.WriteLine($"Amount {i.Amount} Price {i.Price}");
            }
        }
    }

    class OrderBook {
        private SortedSet<Order> buyOrders = new SortedSet<Order>(new ByHighestPriceComparer());
        private SortedSet<Order> sellOrders = new SortedSet<Order>(new ByLowestPriceComparer());

        public event Action<ReadOnlyCollection<Trade>> OnTrade;
        public event Action<Order> OnOrderCancelled;
        public void PlaceOrder(Order order) {
            if (order.IsBuy) {
                buyOrders.Add(order);
            } else {
                sellOrders.Add(order);
            }
        }

        public void CancelOrder(Order order) {
            if (order.IsBuy) {
                buyOrders.Remove(order);
            } else {
                sellOrders.Remove(order);
            }

            OnOrderCancelled?.Invoke(order);
        }

        public void ProcessOrder(Order order) {
            //todo multi-variable switch
            if (order.IsBuy) {
                switch (order.Type)
                {
                    case OrderType.Market: 
                        processOrder(order); 
                        break;
                    case OrderType.Limit: 
                        processOrder(order); 
                        break;
                    default:
                        throw new Exception("unknown order type");
                }
            } else {
                switch (order.Type)
                {
                    case OrderType.Market: 
                        processOrder(order); 
                        break;
                    case OrderType.Limit: 
                        processOrder(order); 
                        break;
                    default:
                        throw new Exception("unknown order type");
                }
            }
        }

        private void processOrder(Order order) {
            var trades = new List<Trade>();

            var ordersToMatch = order.IsBuy ? sellOrders : buyOrders;
            var fulfilledOrdersToMatch = new List<Order>();
            
            foreach (var matchingOrder in ordersToMatch)
            {
                if (order.Type == OrderType.Limit && (
                        order.IsBuy && order.Price < matchingOrder.Price || 
                        !order.IsBuy && order.Price > matchingOrder.Price)) {
                    break;
                }

                int orderAmount;
                if (matchingOrder.Amount >= order.Amount) {
                    orderAmount = order.Amount;
                } else {
                    orderAmount = matchingOrder.Amount;
                }

                trades.Add(new Trade()
                {
                    TakerOrderID = order.Id,
                    MakerOrderID = matchingOrder.Id,
                    Amount = orderAmount,
                    Price = matchingOrder.Price
                });

                order.Amount -= orderAmount;
                matchingOrder.Amount -= orderAmount;

                if (matchingOrder.Amount == 0) {
                    fulfilledOrdersToMatch.Add(matchingOrder);
                }
                if (order.Amount == 0) {
                    if (order.IsBuy)
                        buyOrders.Remove(order);
                    else 
                        sellOrders.Remove(order);

                    break;
                }
            }

            foreach (var fulfilledOrderToMatch in fulfilledOrdersToMatch)
            {
                ordersToMatch.Remove(fulfilledOrderToMatch);
            }

            if (trades.Count > 0)
                OnTrade?.Invoke(trades.AsReadOnly());
        }

        public ReadOnlyCollection<Order> GetBuyOrders() {
            return buyOrders.ToList().AsReadOnly();
        }

        public ReadOnlyCollection<Order> GetSellOrders() {
            return sellOrders.ToList().AsReadOnly();
        }

        private class ByHighestPriceComparer : IComparer<Order>
        {
            public int Compare(Order order1, Order order2)
            {
                return order2.Price.CompareTo(order1.Price);
            }
        }

        private class ByLowestPriceComparer : IComparer<Order>
        {
            public int Compare(Order order1, Order order2)
            {
                return order1.Price.CompareTo(order2.Price);
            }
        }
    }
    class OrderBase {
        public string Id;
        public int Amount;
        public decimal Price;
        public bool IsBuy;

        public OrderType Type;
    }
    class Order : OrderBase {
    }

    class IcebergOrder : OrderBase {
        private string currentlyFulfilledOrderId;
        private int currentlyFulfilledAmount;
        private readonly int splitAmount;
        private OrderBook orderBook;

        public event Action OnFulfilled;
        public IcebergOrder(OrderBook orderBook, int amount, uint splitFactor /*some rule could be here*/) {
            if( splitFactor == 0 )
                throw new ArgumentOutOfRangeException("splitFactor", "parameter splitFactor must be greater than 0");

            this.orderBook = orderBook;
            this.Amount = amount;
            this.splitAmount = (int) Math.Ceiling((decimal)this.Amount / splitFactor);
        }

        ~IcebergOrder() {
            Console.WriteLine("IcebergOrder destructor");
        }

        public void StartFulfilling() {
            orderBook.OnTrade += (trades) => {
                var amountTradedFromThisOrder = trades.Where(t => 
                    t.TakerOrderID == currentlyFulfilledOrderId || 
                    t.MakerOrderID == currentlyFulfilledOrderId)
                .Sum(t => t.Amount);

                if (amountTradedFromThisOrder > 0) {
                    currentlyFulfilledAmount -= amountTradedFromThisOrder; 
                    Amount -= amountTradedFromThisOrder;
                    if (Amount == 0) {
                        OnFulfilled?.Invoke();
                        return;
                    }
                    if (currentlyFulfilledAmount == 0) {
                        placeNewSuborder();
                    }
                }
            };

            placeNewSuborder();
        }
        private void onTradeHandler(ReadOnlyCollection<Trade> trades) {

        }
        private void placeNewSuborder() {
            currentlyFulfilledAmount = this.Amount < this.splitAmount ? this.Amount : this.splitAmount;
            var order = new Order() {
                Type = this.Type,
                Price = this.Price,
                Amount = currentlyFulfilledAmount,
                IsBuy = this.IsBuy
            };
            currentlyFulfilledOrderId = order.Id;
            
            orderBook.PlaceOrder(order);
        }
    }
    
    class Trade {
        public string TakerOrderID;
        public string MakerOrderID;
        public int Amount;
        public decimal Price;
    }

    public enum OrderType {
        Market,
        Limit,
        Stop
    }
}
