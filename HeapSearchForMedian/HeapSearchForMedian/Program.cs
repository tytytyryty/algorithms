﻿using System;
using System.Linq;
using System.IO;

namespace HeapSearchForMedian
{
    class Program
    {
        static void Main(string[] args)
        {
            var heapLow = new Heap(HeapType.NonIncreasing);
            var heapHigh = new Heap(HeapType.NonDecreasing);

            var lines = File.ReadAllLines("input.txt");
            var arr = lines.Select(i => int.Parse(i)).ToArray();

            arr = new[] { 4, 2, 3, 8, 7, 6, 5 };
            //var heap = new Heap(arr, HeapType.NonDecreasing);

            for (int i = 0; i < arr.Length; i++)
            {
                var curr = arr[i];
                if (heapLow.NodesCount() == 0 || curr < heapLow.GetMinMax())
                {
                    heapLow.Insert(curr);
                }
                else
                {
                    heapHigh.Insert(curr);
                }

                if (heapLow.NodesCount() - heapHigh.NodesCount() == 2)
                {
                    var max = heapLow.ExtractMinMax();
                    heapHigh.Insert(max);
                }
                if (heapHigh.NodesCount() - heapLow.NodesCount() == 2)
                {
                    var min = heapHigh.ExtractMinMax();
                    heapLow.Insert(min);
                }

                //2 medians
                if ((i + 1) % 2 == 0)
                {
                    var median1 = heapLow.GetMinMax();
                    var median2 = heapHigh.GetMinMax();
                    Console.WriteLine(median1 + " " + median2);
                }
                //1 median
                else
                {
                    var median = heapLow.NodesCount() > heapHigh.NodesCount()
                        ? heapLow.GetMinMax()
                        : heapHigh.GetMinMax();
                    Console.WriteLine(median);
                }
            }
            
            Console.ReadLine();
        }
    }
}
