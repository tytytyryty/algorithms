﻿
//https://www.programiz.com/dsa/graph-bfs
// 0---3
// |\
// | 2
// |/ \
// 1   4

var node0 = new Node("0");
var node1 = new Node("1");
var node2 = new Node("2");
var node3 = new Node("3");
var node4 = new Node("4");

node0.Children = new Node[] {node1, node2, node3};
node1.Children = new Node[] {node0, node2};
node2.Children = new Node[] {node0, node1, node4};
node3.Children = new Node[] {node0};
node4.Children = new Node[] {node2};
var nodes = new [] {node0, node1, node2, node3, node4};

//0 1 2 3 4
markUnvisited(nodes);
BFS(node0);
Console.WriteLine();

//0 3 2 4 1
markUnvisited(nodes);
DFS(node0);
Console.WriteLine();

static void BFS(Node node) {
    var queue = new Queue<Node>();
    queue.Enqueue(node);
    node.Visited = true;

    while (queue.Count > 0) {
        var current = queue.Dequeue();
        Console.Write(current.Name + " ");
        foreach (var child in current.Children) {
            if (!child.Visited) {
                queue.Enqueue(child);
                child.Visited = true;
            }
        }
    }
}

static void DFS(Node node) {
    var stack = new Stack<Node>();
    stack.Push(node);
    node.Visited = true;

    while (stack.Count > 0) {
        var current = stack.Pop();
        Console.Write(current.Name + " ");
        foreach (var child in current.Children) {
            if (!child.Visited) {
                stack.Push(child);
                child.Visited = true;
            }
        }
    }
}

static void markUnvisited(Node[] nodes) {
    foreach(var node in nodes) {
        node.Visited = false;
    }
}

public class Node {
    public Node (string name) {
        Name = name;
    }
    
    public Node[] Children { get; set; }
    public string Name { get; }
    public bool Visited { get; set; }
}