﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BinaryTree
{
    class BinaryTree
    {
        public virtual BinaryTreeNode Root { get; set; }

        public BinaryTree()
        {
        }

        public BinaryTree(int root) : this(new BinaryTreeNode(root))
        {
        }

        public BinaryTree(BinaryTreeNode root)
        {
            Root = root;
        }

        public int[] TraverseDFSInOrder(Action<BinaryTreeNode> func = null)
        {
            var list = new List<int>();
            TraverseInOrderRecursive(this.Root, list, func);
            return list.ToArray();
        }

        public int[] TraverseBFS(Action<BinaryTreeNode> func = null)
        {
            var list = new List<int>();
            var queue = new Queue<BinaryTreeNode>();
            queue.Enqueue(this.Root);

            while(queue.Any())
            {
                var node = queue.Dequeue();
                list.Add(node.Value);

                if (node.Left != null)
                {
                    queue.Enqueue(node.Left);
                }
                if (node.Right != null)
                {
                    queue.Enqueue(node.Right);
                }
            }

            return list.ToArray();
        }

        private void TraverseInOrderRecursive(BinaryTreeNode node, List<int> list, Action<BinaryTreeNode> func)
        {
            if (node != null)
            {
                //list.Add here is pre-order
                TraverseInOrderRecursive(node.Left, list, func);
                list.Add(node.Value);
                if (func != null)
                {
                    func(node);
                }
                TraverseInOrderRecursive(node.Right, list, func);
                //list.Add here is post-order
            }
        }

        public void TraverseWithActions(Action<BinaryTreeNode> beforeLeft, Action<BinaryTreeNode> between, Action<BinaryTreeNode> afterRight, BinaryTreeNode node = null)
        {
            node = node ?? Root;
            TraverseWithActionsRecoursive(node, beforeLeft, between, afterRight);
        }

        private void TraverseWithActionsRecoursive(BinaryTreeNode node, Action<BinaryTreeNode> beforeLeft, Action<BinaryTreeNode> between, Action<BinaryTreeNode> afterRight)
        {
            if (node != null)
            {
                if (beforeLeft != null)
                {
                    beforeLeft(node);
                }
                TraverseWithActionsRecoursive(node.Left, beforeLeft, between, afterRight);

                if (between != null)
                {
                    between(node);
                }
                TraverseWithActionsRecoursive(node.Right, beforeLeft, between, afterRight);

                if (afterRight != null)
                {
                    afterRight(node);
                }
            }
        }

        public static BinaryTree FromArray(int[] nodes)
        {
            var tree = new BinaryTree(nodes[0]);
            var i = 1;
            tree.TraverseWithActions(
                node => { if (nodes[i] != 0) { node.Left = new Left(nodes[i]); } i++; },
                node => { if (nodes[i] != 0) { node.Right = new Right(nodes[i]); } i++; }, 
                null);
            return tree;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as BinaryTree);
        }

        public bool Equals(BinaryTree tree)
        {
            return EqualsRecursive(Root, tree.Root);
        }

        private bool EqualsRecursive(BinaryTreeNode firstTreeNode, BinaryTreeNode secondTreeNode)
        {
            var isNullFirst = firstTreeNode == null;
            var isNullSecond = secondTreeNode == null;

            if (isNullFirst ^ isNullSecond)
                return false;

            if (!isNullFirst)
            {
                if (firstTreeNode.Value != secondTreeNode.Value)
                    return false;

                var leftRes = EqualsRecursive(firstTreeNode.Left, secondTreeNode.Left);
                if (leftRes == false)
                    return false;
                var rightRes = EqualsRecursive(firstTreeNode.Right, secondTreeNode.Right);
                if (rightRes == false)
                    return false;
            }

            return true;
        }

        public BinaryTree Clone()
        {
            var clone = CreateInstanceForClone(this.Root.Value);
            CloneRecursive(this.Root, clone.Root);
            return clone;
        }

        protected virtual BinaryTree CreateInstanceForClone(int root)
        {
            return new BinaryTree(root);
        }

        private void CloneRecursive(BinaryTreeNode original, BinaryTreeNode clone)
        {
            if (original.Left != null)
            {
                clone.Left = new Left(original.Left.Value);
                CloneRecursive(original.Left, clone.Left);
            }
            if (original.Right != null)
            {
                clone.Right = new Right(original.Right.Value);
                CloneRecursive(original.Right, clone.Right);
            }
        }
    }
}
