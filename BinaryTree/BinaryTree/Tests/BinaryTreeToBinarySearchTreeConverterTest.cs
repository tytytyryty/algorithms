﻿using NUnit.Framework;

namespace BinaryTree
{
    [TestFixture]
    class BinaryTreeToBinarySearchTreeConverterTest
    {
        //              1
        //       4            2
        //    6     7      5     3
        // 10         8        9

        private readonly BinaryTree initialBinaryTree = new BinaryTree
        {
            Root = new BinaryTreeNode(
                    1,
                    new Left(4,
                        new Left(6,
                            new Left(10)),
                        new Right(7,
                            new Right(8))
                        ),
                    new Right(2,
                        new Left(5),
                        new Right(3,
                            new Left(9))
                        )
                    )
        };
        
        //              6
        //       3            8
        //    2     4      7     10
        //  1         5        9

        private BinaryTree expectedBinarySearchTree = new BinarySearchTree
        {
            Root = new BinaryTreeNode(
                    6,
                    new Left(3,
                        new Left(2,
                            new Left(1)),
                        new Right(4,
                            new Right(5))
                        ),
                    new Right(8,
                        new Left(7),
                        new Right(10,
                            new Left(9))
                        )
                    )
        };

        [Test]
        public void ConvertWorksCorrectly()
        {
            //arrange
            var btConvertor = new BinaryTreeToBinarySearchTreeConverter();
            
            //act
            var resultBinarySearchTree = btConvertor.Convert(initialBinaryTree);
           
            //assert
            Assert.IsTrue(expectedBinarySearchTree.Equals(resultBinarySearchTree));
        }
        
    }
}
