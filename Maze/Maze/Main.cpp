#include <iostream>
#include "MazeGenerator.h"
#include "MazePrinter.h"
#include "Maze.h"
#include <windows.h>
#include "MazeSolver.h"
using namespace std;

void print(Maze maze);
void clearScreen();

int main()
{
	cout << "Maze generator & solver" << endl;
	MazeGenerator mazeGenerator;
	MazeSolver mazeSolver;
	MazePrinter mazePrinter;

	mazeGenerator.OnInterimGenerationStep(print);
	Maze maze = mazeGenerator.Generate(10, 10, true);
	
	system("cls");
	cout << "Generated maze:" << endl;
	mazePrinter.Print(maze);

	cout << "Press enter to solve" << endl;
	cin.get();
	system("cls");

	mazeSolver.OnInterimSolvingStep(print);
	MazePath mazePath = mazeSolver.Solve(maze);

	cout << "Solved maze:" << endl;
	mazePrinter.Print(maze);

	cout << "Path:" << endl;
	cout << mazePath;
}

void print(Maze maze) 
{
	Sleep(200);
	//system("cls");
	//to avoid flickering
	clearScreen();
	MazePrinter mazePrinter;
	mazePrinter.Print(maze);
}

void clearScreen()
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD Position;

	Position.X = 0;
	Position.Y = 0;

	SetConsoleCursorPosition(hOut, Position);
}