﻿// https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
printPermutations("ABC");

static void printPermutations(string str) {
    //A
    //AB
    //ABC  ABD
    //ABCD ABDC

    permute(str, "", str);
}

//Time Complexity: O(N * N!) Note that there are N! permutations and it requires O(N) time to print a permutation.
static void permute(string str, string builtPart, string permutePart) {
    if (str.Length == builtPart.Length) {
        Console.WriteLine(builtPart);
    }
    
    for (int i=0; i<permutePart.Length; i++) {
        var currentChar = permutePart[i];
        var beforeChar = permutePart.Substring(0,i);
        var afterChar = permutePart.Substring(i+1);
        permute(str, builtPart+currentChar, beforeChar+afterChar);
    }
}