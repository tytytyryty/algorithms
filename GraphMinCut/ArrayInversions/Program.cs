﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArrayInversions
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<Vertex> vertexList = new List<Vertex>();
            string[] strArray = File.ReadAllLines("AdjacencyList.txt");
            //strArray = new[] {"1 2 3", "2 1 3 4", "3 1 2 4", "4 2 3"};
            foreach (var str in strArray)
            {
                var currVertexStr = str.Split(' ');
                var currVertex = new Vertex {VertexNumber = int.Parse(currVertexStr[0]) - 1 };
                for (int i = 1; i < currVertexStr.Length; i++)
                {
                    currVertex.Adjacences.Add(int.Parse(currVertexStr[i]) - 1);
                }
                vertexList.Add(currVertex);
            }

            foreach (var vertex in vertexList)
            {
                Console.Write(vertex.VertexNumber);
                foreach (var edge in vertex.Adjacences)
                {
                    Console.Write(" " + edge);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            //for (int i = 0; i < 1000; i++)
            //{
                Console.WriteLine(CalcMinCut(vertexList));
            //}
            
            Console.ReadKey(true);
        }

        public static int CalcMinCut(List<Vertex> vertexList)
        {
            Random rand = new Random();
            while (true)
            {
                var stillInList = vertexList.Where(i => i.IsRemoved == false).ToList();
                var countInList = stillInList.Count;
                if (countInList > 2)
                {
                    
                    var randVertex = vertexList[rand.Next(0, vertexList.Count - 1)];
                    var randAdjacences = vertexList[randVertex.Adjacences[rand.Next(0, randVertex.Adjacences.Count - 1)]];
                    if (randVertex.VertexNumber == randAdjacences.VertexNumber || randVertex.IsRemoved || randAdjacences.IsRemoved)
                    {
                        continue;
                    }

                    randVertex.Adjacences.RemoveAll(i => i == randAdjacences.VertexNumber);
                    randAdjacences.Adjacences.Remove(randVertex.VertexNumber);

                    var c = randAdjacences.Adjacences.Count;
                    for (int i = 0; i < c; i++)
                    {
                        randVertex.Adjacences.Add(randAdjacences.Adjacences[i]);
                        vertexList[randAdjacences.Adjacences[i]].Adjacences.Add(randVertex.VertexNumber);
                        vertexList[randAdjacences.Adjacences[i]].Adjacences.Remove(randAdjacences.VertexNumber);
                        //if (randAdjacences.Adjacences[i] != randVertex.VertexNumber)
                        //{
                        //    randVertex.Adjacences.Add(randAdjacences.Adjacences[i]);
                        //}
                        //if (randVertex.VertexNumber != vertexList[randAdjacences.Adjacences[i]].VertexNumber)
                        //{
                        //    vertexList[randAdjacences.Adjacences[i]].Adjacences.Add(randVertex.VertexNumber);
                        //}
                    }
                    //vertexList.Remove(randAdjacences);  
                    //vertexList[randAdjacences.VertexNumber].Adjacences.Clear();
                    foreach (var vertex in vertexList)
                    {
                        for (int i=0; i<vertex.Adjacences.Count; i++)
                        {
                            if (vertex.Adjacences[i] == vertex.VertexNumber)
                            {
                                vertex.Adjacences.RemoveAll(j => j == vertex.VertexNumber);
                            }
                        }
                    }
                    vertexList[randAdjacences.VertexNumber].IsRemoved = true;
                }
                else
                {
                    Vertex maxAdj = vertexList[0];
                    foreach (var vertex in vertexList)
                    {
                        if (vertex.IsRemoved == false)
                        {
                            maxAdj = vertex;
                            break;
                        }   
                    }
                    foreach (var vertex in vertexList)
                    {
                        if (vertex.IsRemoved == false)
                        {
                            //Console.Write(vertex.VertexNumber);
                            //foreach (var edge in vertex.Adjacences)
                            //{
                            //    Console.Write(" " + edge);
                            //}
                            //Console.WriteLine();
                            //Console.WriteLine();
                        }
                    }
                    return maxAdj.Adjacences.Count;
                }
                
            }
        }
    }

    class Vertex
    {
        public int VertexNumber;
        public List<int> Adjacences = new List<int>();
        public bool IsRemoved = false;
    }
}
