﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeConsoleOutputLib
{
    public class TreeConsoleOutputHelper
    {
        private int[] _nodes;
        private int _baseSpaceCount;
        private ArrowsOutputType _outputType;
        private int _nodesCount;
        private int _treeDepth;

        private StringBuilder _currLevelStr = new StringBuilder();
        private StringBuilder _currLevelArrowsStr = new StringBuilder();

        public TreeConsoleOutputHelper(int[] nodes, ArrowsOutputType outputType = ArrowsOutputType.None, int baseSpaceCount = 1)
        {
            _nodes = nodes;
            _baseSpaceCount = baseSpaceCount;
            _outputType = outputType;
            _nodesCount = nodes.Length;
            _treeDepth = GetTreeDepth(_nodesCount);
        }

        public void Output()
        {
            int nodeLevel = 0;
            int spaceCountBeforeNode = 0;
            
            for (int i = 0; i < _nodes.Length; i++)
            {
                int currNodeNumber = i + 1;
                int currNodeLevel = GetTreeDepth(currNodeNumber);

                if (currNodeLevel > nodeLevel)
                {
                    OutputCurrentLevel();
                    nodeLevel = currNodeLevel;

                    spaceCountBeforeNode = (int) Math.Pow(2, (_treeDepth - currNodeLevel)) - 1;
                    WriteNode(_nodes[i], currNodeNumber, spaceCountBeforeNode, SpaceType.Before);
                }
                else
                {
                    int spaceCountBetweenNodes = 2 * spaceCountBeforeNode + 1;
                    WriteNode(_nodes[i], currNodeNumber, spaceCountBetweenNodes, SpaceType.Between);
                }
            }

            OutputCurrentLevel();
        }

        private void WriteNode(int node, int nodeNumber, int spaceCountForNode, SpaceType spaceType)
        {
            WriteNodeWithSpaces(node, spaceCountForNode);
            
            if (_outputType == ArrowsOutputType.Before)
            {
                WriteArrowsBeforeWithSpaces(nodeNumber, spaceCountForNode, spaceType);
            }
            if (_outputType == ArrowsOutputType.After)
            {
                WriteArrowsAfterWithSpaces(nodeNumber, spaceCountForNode, spaceType);
            }
        }
        
        private enum SpaceType
        {
            Before,
            Between
        }

        private void OutputCurrentLevel()
        {
            if (_outputType == ArrowsOutputType.Before)
            {
                OutputArrowsLevel();
                OutputNodesLevel();
            }
            if (_outputType == ArrowsOutputType.After)
            {
                OutputNodesLevel();
                OutputArrowsLevel();
            }
            if (_outputType == ArrowsOutputType.None)
            {
                OutputNodesLevel();
            }

            _currLevelStr.Clear();
            _currLevelArrowsStr.Clear();
        }

        private void OutputNodesLevel()
        {
            if (_currLevelStr.Length > 0)
            {
                Console.WriteLine(_currLevelStr.ToString());
            }
        }

        private void OutputArrowsLevel()
        {
            if (_currLevelArrowsStr.Length > 0)
            {
                Console.WriteLine(_currLevelArrowsStr.ToString());
            }
        }

        private void WriteNodeWithSpaces(int node, int spaceCount)
        {
            _currLevelStr.Append("".PadRight(spaceCount * _baseSpaceCount, ' '));
            _currLevelStr.Append(node);
        }

        private void WriteArrowsAfterWithSpaces(int nodeNumber, int spaceCountForNode, SpaceType spaceType)
        {
            var spaceCountForArrows = spaceType == SpaceType.Before ? spaceCountForNode - 1 : spaceCountForNode - 2;
            if (HasLeftLeave(nodeNumber))
            {
                _currLevelArrowsStr.Append("".PadRight(spaceCountForArrows * _baseSpaceCount, ' '));
                _currLevelArrowsStr.Append(@"/");
            }
            if (HasRightLeave(nodeNumber))
            {
                _currLevelArrowsStr.Append(@" \");
            }
        }

        private void WriteArrowsBeforeWithSpaces(int nodeNumber, int spaceCountForNode, SpaceType spaceType)
        {
            var spaceCountForArrows = spaceCountForNode;
            if (nodeNumber != 1)
            {
                _currLevelArrowsStr.Append("".PadRight(spaceCountForArrows * _baseSpaceCount, ' '));
                if (nodeNumber % 2 == 0)
                {
                    _currLevelArrowsStr.Append(@"/");
                }
                else
                {
                    _currLevelArrowsStr.Append(@"\");
                }
            }
        }
        
        private int GetTreeDepth(int nodesCount)
        {
            return (int)Math.Log(nodesCount, 2) + 1;
        }

        private bool HasLeftLeave(int nodeNumber)
        {
            return nodeNumber * 2 <= _nodesCount;
        }

        private bool HasRightLeave(int nodeNumber)
        {
            return nodeNumber * 2 + 1 <= _nodesCount;
        }
    }
}
