#include "Maze.h"
#include "MazePath.h"
#include "EmptyMazePath.h"
#include "NeighbourCell.h"
#pragma once
class MazeSolver
{
public:
	MazeSolver();
	~MazeSolver();
	MazePath Solve(Maze maze);
	void OnInterimSolvingStep(void (*interimSolvingStep)(Maze maze));
private: 
	static const int CELL_NEIGHBOURS_COUNT = 4;
	void (*interimSolvingStep)(Maze maze);
	pair<NeighbourCell*, int> getNeighboursToGo(Point const& point, Maze& maze);
};

