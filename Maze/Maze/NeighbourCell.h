#include "Direction.h"
#include "Point.h"
#pragma once
struct NeighbourCell
{
	NeighbourCell()
	{

	}

	NeighbourCell(Point point, Direction direction) 
	{
		this->point = point;
		this->direction = direction;
	}

	Direction direction;
	Point point;
}; 
