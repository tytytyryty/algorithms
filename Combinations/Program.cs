﻿// https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
printCombinations(new int[] {1, 2, 3, 4, 5}, 3);

static void printCombinations(int[] arr, int size) {
    //1 2 3
    //1 2 4
    //1 2 5
    //1 3 4
    //1 3 5
    //1 4 5
    //2 3 4
    //2 3 5
    //2 4 5
    //3 4 5
    Array.Sort(arr);
    
    combine(arr, size, new List<int>() {}, 0, 0 + size - 1);
}

//1. we could always pass as "end" the last index of the array, here we optimize for not possible cases 
//2. we create new list every recursion but not all of them are used since they are destroyed during recursion
//   it's possible to optimize and use only single array and pass additional index parameter to the recursion
static void combine(int[] arr, int size, List<int> data, int start, int end) {
    if (data.Count == size) {
        printCurrCombination(data);
        return;
    }

    for (int i=start; i<=end; i++) {
        var dataCopy = new List<int>(data) { arr[i] };
        combine(arr, size, dataCopy, i+1, dataCopy.Count + size - 1);
    }
}

static void printCurrCombination (List<int> currCombination) {
    foreach (var part in currCombination) {
        Console.Write(part + " ");
    }
    Console.WriteLine();
}