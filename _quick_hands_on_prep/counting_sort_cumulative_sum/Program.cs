﻿int [] items = new int[] {5, 1, 4, 8, 2, 5};
CountingSort(items);

// 0 1 2 3 4 5 6 7 8 9
// 0 1 1 0 1 2 0 0 1 0 counts
// 0 1 2 2 3 5 5 5 6 6 cumulative sum
// 0 0 1 2 2 3 5 5 5 6 shifted cumulative sum
void CountingSort(int[] items) {
    var counts = new int[10];
    for (int i = 0; i < items.Length; i++)
    {
        counts[items[i]]++;
    }
    //if no order preserving required, just output each present item multiplying by it's count
    for (int i = 0; i < 10; i++)
    {
        if (counts[i] > 0) {
            for (int j=0; j<counts[i]; j++) {
                Console.Write(i + " ");
            }
        }
    }
    Console.WriteLine();
    //if order preserving required - sort through cumulative sum, which correlate to starting index of each item
    int sum = 0;
    for (int i = 0; i < 10; i++)
    {
        sum += counts[i];
        counts[i] = sum;
    }
    //shift array to the right once to have proper indexes
    for (int i = counts.Length - 1; i >= 1; i--)
    {
        counts[i] = counts[i-1];
    }
    counts[0] = 0;
    //fill in output array
    var output = new int[items.Length];
    for (int i = 0; i < items.Length; i++)
    {
        var outputIndex = counts[items[i]];
        output[outputIndex] = items[i];
        //increase output index for the next same element
        counts[items[i]]++;
    }
    //output final filled array
    for (int i = 0; i < output.Length; i++)
    {
        Console.Write(output[i] + " ");
    }
    Console.WriteLine();
}