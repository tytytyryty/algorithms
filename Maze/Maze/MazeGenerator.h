
#include <time.h>
#include <stdio.h>

#include <stack> 
#include <tuple>
#include "Maze.h"
#include "NeighbourCell.h"
#include <stdlib.h>
using namespace std;

#pragma once
class MazeGenerator
{
	public:
		MazeGenerator();
		~MazeGenerator();
		Maze Generate(int height, int width, bool onePath);
		void OnInterimGenerationStep(void (*interimGenerationStep)(Maze maze));

	private:
		static const int CELL_NEIGHBOURS_COUNT = 4;
		void (*interimGenerationStep)(Maze maze);
		pair<NeighbourCell*, int> getNeighboursToGo(Point const& point, Maze& maze);
		Point* getNeighbours(Point const &point);
		bool cellIsWall(Point &cell, Maze &maze);
		bool cellIsNotBorder(Point &cell, Maze &maze);
		bool noMoreThanOneCellNeighbourIsPath(Point &cell, Maze &maze);
};

