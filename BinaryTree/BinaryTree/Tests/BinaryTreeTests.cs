﻿using NUnit.Framework;

namespace BinaryTree
{
    [TestFixture]
    class BinaryTreeTests
    {
        private readonly BinaryTree initialBinaryTree = new BinaryTree
        {
            Root = new BinaryTreeNode(
                    1,
                    new Left(4,
                        new Left(6,
                            new Left(10)),
                        new Right(7,
                            new Right(8))
                        ),
                    new Right(2,
                        new Left(5),
                        new Right(3,
                            new Left(9))
                        )
                    )};

        [Test]
        public void TraverseDFSInOrderWorksCorrectly()
        {
            //arrange
            var expectedArr = new[] {10, 6, 4, 7, 8, 1, 5, 2, 9, 3};

            //act
            var result = initialBinaryTree.TraverseDFSInOrder();

            //assert
            CollectionAssert.AreEqual(expectedArr, result);
        }

        [Test]
        public void TraverseBFSWorksCorrectly()
        {
            //arrange
            var expectedArr = new[] { 1, 4, 2, 6, 7, 5, 3, 10, 8, 9 };

            //act
            var result = initialBinaryTree.TraverseBFS();

            //assert
            CollectionAssert.AreEqual(expectedArr, result);
        }

        [Test]
        public void FromArrayWorksCorrectly()
        {
            //arrange
            var arr = new[] { 1, 4, 6, 10, 0, 0, 0, 7, 0, 8, 0, 0, 2, 5, 0, 0, 3, 9, 0, 0, 0 };

            //act
            var result = BinaryTree.FromArray(arr);

            //assert
            Assert.IsTrue(result.Equals(initialBinaryTree));
        }

        [Test]
        public void CloneWorksCorrectly()
        {
            //arrange

            //act
            var result = initialBinaryTree.Clone();

            //assert
            Assert.IsTrue(result.Equals(initialBinaryTree));
        }

        [Test]
        public void EqualsReturnsTrueForIdenticalTrees()
        {
            //arrange
            var clonedTree = new BinaryTree(initialBinaryTree.Root);

            //act
            var result = clonedTree.Equals(initialBinaryTree);

            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void EqualsReturnsFalseForDifferentLeftTree()
        {
            //arrange
            var anotherTree = new BinaryTree(
                new BinaryTreeNode(
                    1, 
                    new Left(5), 
                    new Right(2,
                        new Left(5),
                        new Right(3,
                            new Left(9))
                        )
                    ));

            //act
            var result = anotherTree.Equals(initialBinaryTree);

            //assert
            Assert.IsFalse(result);
        }

        [Test]
        public void EqualsReturnsFalseWhenOnlyOneNullLeafDiffer()
        {
            //arrange
            var anotherTree = new BinaryTree
            {
                Root = new BinaryTreeNode(
                    1,
                    new Left(4,
                        new Left(6,
                            new Left(10)),
                        new Right(7,
                            (Left)null)
                        ),
                    new Right(2,
                        new Left(5),
                        new Right(3,
                            new Left(9))
                        )
                    )};

            //act
            var result = anotherTree.Equals(initialBinaryTree);

            //assert
            Assert.IsFalse(result);
        }

        [Test]
        public void EqualsReturnsFalseForNullRightTree()
        {
            //arrange
            var anotherTree = new BinaryTree(new BinaryTreeNode(1, new Left(7), null));

            //act
            var result = anotherTree.Equals(initialBinaryTree);

            //assert
            Assert.IsFalse(result);
        }
    }
}
