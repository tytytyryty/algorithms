﻿
Console.WriteLine("start");

//      1
var root0 = new Node(); //1

//      1
//     / \
//    2   3
//   / \
//  4   5
var root1 = new Node(); //1
root1.left = new Node(); //2
root1.left.left = new Node(); //4
root1.left.right = new Node(); //5
root1.right = new Node(); //3

//        1
//       / \
//      2   3
//     / \
//    4   5
//   /     \
//  6       7
var root2 = new Node(); //1
root2.left = new Node(); //2
root2.left.left = new Node(); //4
root2.left.left.left = new Node(); //6
root2.left.right = new Node(); //5
root2.left.right.right = new Node(); //7
root2.right = new Node(); //3

//        1
//       / \
//      2   3
//     / \
//    4   5
//   /     \
//  6       7
// /         \
//8           9
var root3 = new Node(); //1
root3.left = new Node(); //2
root3.left.left = new Node(); //4
root3.left.left.left = new Node(); //6
root3.left.left.left.left = new Node(); //8
root3.left.right = new Node(); //5
root3.left.right.right = new Node(); //7
root3.left.right.right.right = new Node(); //9
root3.right = new Node(); //3      

Console.WriteLine($"h: {findHeight(root0)} d: {findDiameter(root0)}");
Console.WriteLine($"h: {findHeight(root1)} d: {findDiameter(root1)}");
Console.WriteLine($"h: {findHeight(root2)} d: {findDiameter(root2)}");
Console.WriteLine($"h: {findHeight(root3)} d: {findDiameter(root3)}");

Console.WriteLine("finish");

static int findHeight(Node node)
{
    return findHeightRecursive(node);
}

static int findHeightRecursive(Node node)
{
    if (node == null)
    {
        return -1;
    }

    int leftHeight = findHeightRecursive(node.left);
    int rightHeight = findHeightRecursive(node.right);

    return Math.Max(leftHeight, rightHeight) + 1;
}

static int findDiameter(Node node)
{
    int diameter = 0;
    findDiameterRecursive(node, ref diameter);
    return diameter;
}

//      1
//     / \
//    2   3
//   / \
//  4   5
//static int diameter = 0;
static int findDiameterRecursive(Node node, ref int diameter)
{
    if (node == null)
        return -1;

    int heightLeft = findDiameterRecursive(node.left, ref diameter);
    int heightRight = findDiameterRecursive(node.right, ref diameter);
    int height = Math.Max(heightLeft, heightRight) + 1;
    diameter = Math.Max(diameter, heightLeft + heightRight + 2);
    return height;
}

class Node
{
    public Node left;
    public Node right;
    public int value;
}
