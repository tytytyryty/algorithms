﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueensLocalSearch
{
    public class QueensProblem
    {
        public QueensProblem()
        {
              
        }

        public event PositionChangedHandler PositionChanged;
        public delegate void PositionChangedHandler(object sender, char[,] position);

        public event ProblemSolvedHandler ProblemSolved;
        public delegate void ProblemSolvedHandler();

        public int StartFromRow { get; set; }

        private const int _boardLength = 8;

        public char[,] InitBoard()
        {
            char [,] board = new char[_boardLength, _boardLength];

            Random rand = new Random();

            var nonUsedCols = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };

            for (int i = 0; i < _boardLength; i++)
            {
                for (int j = 0; j < _boardLength; j++)
                {
                    board[i, j] = '.';
                }

                var randCol = rand.Next(0, nonUsedCols.Count);
                board[i, nonUsedCols[randCol]] = 'Q';
                nonUsedCols.Remove(nonUsedCols[randCol]);
            }

            return board;
        }

        public void OutputBoard(char[,] position)
        {
            Console.WriteLine("  1 2 3 4 5 6 7 8");
            for (int i = 0; i < _boardLength; i++)
            {
                Console.Write((i+1) + " ");
                for (int j = 0; j < _boardLength; j++)
                {
                    Console.Write(position[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public void LocalSearch()
        {
            char[,] bestQueensPosition = InitBoard(); 
            int bestBeatCount = getBeatCount(bestQueensPosition);

            int iteration = 0;

            //for (iteration = 0; iteration < 100000; iteration++)
            while (true)
            {
                char[,] currQueensPosition = getNextPosition(bestQueensPosition);
                int currBeatCount = getBeatCount(currQueensPosition);
                
                if (currBeatCount == 0)
                {
                    bestQueensPosition = currQueensPosition;
                    bestBeatCount = currBeatCount;
                    break;
                }
                
                if (currBeatCount < bestBeatCount)
                {
                    bestBeatCount = currBeatCount;
                    bestQueensPosition = currQueensPosition;
                    
                }
                PositionChanged(this, bestQueensPosition);
                iteration++;
            }
            PositionChanged(this, bestQueensPosition);
            ProblemSolved();
            ///OutputBoard(bestQueensPosition);
            //Console.WriteLine("Beat count: {0}", bestBeatCount / 2);
            //Console.WriteLine("Iteration: {0}", iteration);
        }

        public void Backtracking()
        {
            char[,] currentBoard = new char[_boardLength, _boardLength];
            for (int i = 0; i < _boardLength; i++)
            {
                for (int j = 0; j < _boardLength; j++)
                {
                    currentBoard[i, j] = '.';
                }   
            }

            int iteration = 0;
            //начинаем со второй колонки, так как на первую уже поставили ферзяыыыы
            int currentCol = 1;
            //ставим ферзя в определенную строку первой колонки
            currentBoard[StartFromRow, 0] = 'Q';
            PositionChanged(this, currentBoard);
            int lastQRow = 0;
            int lastQCol = 0;
            int maxColCount = _boardLength;
            List<Position> tabuList = new List<Position>();

            while (true)
            {
                bool hasColSolution = false;
                int startFromRow = 0;
                var lastTabuRow = tabuList.FindLast((a)=>a.Col == currentCol);
                if (lastTabuRow != null)
                {
                    startFromRow = lastTabuRow.Row + 1;
                }
                for (int i = startFromRow; i < _boardLength; i++)
                {
                    currentBoard[i, currentCol] = 'Q';
                    PositionChanged(this, currentBoard);
                    int beatCount = getQueenBeat(i, currentCol, currentBoard);
                    if (beatCount == 0)
                    {
                        lastQRow = i;
                        lastQCol = currentCol;
                        
                        var newTabuList = new List<Position>();
                        foreach (var position in tabuList)
                        {
                            if (position.Col <= currentCol)
                            {
                                newTabuList.Add(position);
                            }
                        }
                        tabuList.Clear();
                        tabuList = newTabuList;

                        hasColSolution = true;
                        break;
                    }
                    else
                    {
                        currentBoard[i, currentCol] = '.';
                        PositionChanged(this, currentBoard);
                    }
                    
                }

                if (hasColSolution == true)
                {
                    currentCol++;
                    if (currentCol == _boardLength)
                    {
                        break;
                    }

                }
                else
                {
                    if (currentCol != 0)
                    {
                        currentCol--;
                    }

                    lastQCol = currentCol;
                    for (int row = 0; row < _boardLength; row++)
                    {
                        if (currentBoard[row, lastQCol] == 'Q')
                        {
                            lastQRow = row;
                            break;
                        }
                    }

                    currentBoard[lastQRow, lastQCol] = '.';
                    tabuList.Add(new Position() { Row = lastQRow, Col = lastQCol });

                }
                PositionChanged(this, currentBoard);
                iteration++;
                //OutputBoard(currentBoard);
            }
            PositionChanged(this, currentBoard);
            ProblemSolved();
            //OutputBoard(currentBoard);
            //Console.WriteLine("Iteration: {0}", iteration);
        }

        

        

        private int getBeatCount(char[,] position)
        {
            int beatCount = 0;

            for (int i = 0; i < _boardLength; i++)
            {
                for (int j = 0; j < _boardLength; j++)
                {
                    if (position[i, j] == 'Q')
                    {
                        beatCount += getQueenBeat(i, j, position);
                    }
                }    
            }
            return beatCount;
        }

        private int getQueenBeat(int queenRow, int queenCol, char[,] position)
        {
            int beat = 0;
            for (int i = 0; i < _boardLength; i++)
            {
                for (int j = 0; j < _boardLength; j++)
                {
                    if (position[i, j] == 'Q')
                    {
                        if ((i == queenRow || j == queenCol || Math.Abs(queenRow - i) == Math.Abs(queenCol - j)) 
                            && (queenRow != i || queenCol != j))
                        {
                            beat++;
                        }
                    }
                }
            }
            return beat;
        }

        private char[,] getNextPosition(char[,] position)
        {
            var newPosition = position;
            Random rand = new Random();

            int row1 = rand.Next(0, 7);
            int col1 = 0;

            int row2 = rand.Next(0, 7);
            int col2 = 0;

            for (int i = 0; i < _boardLength; i++)
            {
                if (newPosition[row1, i] == 'Q')
                {
                    col1 = i;
                }
                if (newPosition[row2, i] == 'Q')
                {
                    col2 = i;
                }
            }

            newPosition[row1, col1] = '.';
            newPosition[row2, col2] = '.';

            newPosition[row1, col2] = 'Q';
            newPosition[row2, col1] = 'Q';


            return newPosition;
        }
    }

    public class Position
    {
        public int Row;
        public int Col;
    }

    public static class ListPositionExtension
    {
        public static bool ContainsPosition(this List<Position> list, Position pos)
        {
            foreach (var position in list)
            {
                if (position.Row == pos.Row && position.Col == pos.Col)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
