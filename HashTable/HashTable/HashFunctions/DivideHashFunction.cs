﻿using System;

namespace HashTable.HashFunctions
{
    public class DivideHashFunction : IHashFunction
    {
        public long Hash(long key, long m)
        {
            //h(k) = k mod m
            return Math.Abs(key) % m;
        }

        public long Hash(long key, long m, long i)
        {
            return Hash(key, m);
        }
    }
}
