﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace HeapSearchForMedian
{
    class Heap
    {
        List<Node> _nodes;
        HeapType _heapType;
        
        public Heap(int[] array, HeapType heapType)
        {
            _heapType = heapType;
            buildHeap(array);
        }

        public Heap(HeapType heapType)
        {
            _heapType = heapType;
            buildEmptyHeap();
        }

        public int NodesCount()
        {
            return _nodes.Count;
        }

        public int GetMinMax()
        {
            return _nodes.First().Value;
        }

        public int ExtractMinMax()
        {
            var first = _nodes.First();
            var last = _nodes.Last();

            var extractedValue = first.Value;

            first.Value = last.Value;
            _nodes.Remove(last);
            heapify(first);

            return extractedValue;
        }

        public void Insert(int value)
        {
            //var insertableNode = new Node(getInsertIndex(), value);
            //_nodes.Add(insertableNode);

            //var currCheckedNode = insertableNode;
            //var parentIndex = parent(insertableNode.Index);
            //var parentNode = getNode(parentIndex);

            //while (nodeExists(parentIndex) && (
            //        _heapType == HeapType.NonIncreasing && parentNode.Value < currCheckedNode.Value
            //     || _heapType == HeapType.NonDecreasing && parentNode.Value > currCheckedNode.Value))
            //{
            //    swapValues(parentNode, currCheckedNode);

            //    //now check if parent node heapified
            //    currCheckedNode = parentNode;
            //    //getting parent of parent
            //    parentIndex = parent(parentNode.Index);
            //    parentNode = getNode(parentIndex);
            //}

            var insertableNode = new Node(getInsertIndex(), value);
            _nodes.Add(insertableNode);

            var index = insertableNode.Index;

            while (index > 1 && (
                    _heapType == HeapType.NonIncreasing && getNode(parent(index)).Value < getNode(index).Value
                 || _heapType == HeapType.NonDecreasing && getNode(parent(index)).Value > getNode(index).Value))
            {
                swapValues(getNode(parent(index)), getNode(index));
                index = parent(index);
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            var queue = new Queue<Node>();
            queue.Enqueue(_nodes.First());
            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                if (isPowerOfTwo(node.Index))
                {
                    result.AppendLine();    
                }
                result.Append(node.Value);

                var leftIndex = left(node.Index);
                var rightIndex = right(node.Index);

                if (nodeExists(leftIndex))
                {
                    queue.Enqueue(getNode(leftIndex));
                }
                if (nodeExists(rightIndex))
                {
                    queue.Enqueue(getNode(rightIndex));
                }
            }
            return result.ToString();
        }

        private void buildHeap(int[] array)
        {
            _nodes = new List<Node>();
            for (int i = 0; i < array.Length; i++)
            {
                _nodes.Add(new Node(i + 1, array[i]));
            }

            for (var i = _nodes.Count / 2 - 1; i >= 0; i--)
            {
                heapify(_nodes[i]);
            }
        }

        private void buildEmptyHeap()
        {
            _nodes = new List<Node>();
        }

        private void heapify(Node node)
        {
            var leftIndex = left(node.Index);
            var rightIndex = right(node.Index);
            
            var swapNode = node;
            if (nodeExists(leftIndex))
            {
                var leftNode = getNode(leftIndex);
                if (_heapType == HeapType.NonIncreasing && leftNode.Value > swapNode.Value
                    || _heapType == HeapType.NonDecreasing && leftNode.Value < swapNode.Value)
                {
                    swapNode = leftNode;
                }
            }
            if (nodeExists(rightIndex))
            {
                var rightNode = getNode(rightIndex);
                if (_heapType == HeapType.NonIncreasing && rightNode.Value > swapNode.Value
                    || _heapType == HeapType.NonDecreasing && rightNode.Value < swapNode.Value)
                {
                    swapNode = rightNode;
                }
            }

            if (swapNode != node)
            { 
                swapValues(node, swapNode);
                heapify(swapNode);
            }
        }

        private int parent(int index)
        {
            return index / 2;
        }

        private int left(int index)
        {
            return index * 2;
        }

        private int right(int index)
        {
            return left(index) + 1;
        }

        private Node getNode(int index)
        {
            return _nodes.FirstOrDefault(i => i.Index == index);
        }
        
        private bool nodeExists(int index)
        {
            return index > 0 && index <= _nodes.Count;
        }

        private bool needSwap(int parentValue, int childValue)
        {
            return _heapType == HeapType.NonDecreasing && parentValue > childValue ||
                   _heapType == HeapType.NonIncreasing && parentValue < childValue;
        }

        private void swapValues(Node parentValue, Node childValue)
        {
            var tmp = parentValue.Value;
            parentValue.Value = childValue.Value;
            childValue.Value = tmp;
        }

        private int getInsertIndex()
        {
            if (_nodes.Count == 0)
            {
                return 1;
            }

            return _nodes.Max(i => i.Index) + 1;
        }

        private bool isPowerOfTwo(int number)
        {
            while (number % 2 == 0)
            {
                number = number / 2;
            }

            return number == 1;
        }
    }
}
