﻿using HashTable.HashFunctions;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Text;

namespace HashTable
{
    class OpenAddressingHashSet : IHashSet
    {
        private long _m;
        private Bucket[] _hashSet;
        private IProbingHashFunction _hashFunction;

        public OpenAddressingHashSet(long size, IProbingHashFunction hashFunction)
        {
            _m = size;
            _hashSet = new Bucket[size];
            _hashFunction = hashFunction;
        }

        public void Add(long key)
        {
            long i = 0;
            while (i != _m)
            {
                var h = hash(key, i);
                if (_hashSet[h] == null || _hashSet[h].IsRemoved)
                {
                    _hashSet[h] = new Bucket() { Key = key };
                    return;
                }
                i ++;
            }
            throw new Exception("Hash table is full");
        }

        public bool Contains(long key)
        {
            long i = 0;
            while (i != _m)
            {
                var h = hash(key, i);
                if (_hashSet[h] == null)
                {
                    return false;
                }
                if (_hashSet[h].Key == key)
                {
                    return true;
                }
                i ++; 
            }
            return false;
        }

        public void Remove(long key)
        {
            long i = 0;
            while (i != _m)
            {
                var h = hash(key, i);
                if (_hashSet[h] == null || _hashSet[h].IsRemoved)
                {
                    return;
                }
                if (_hashSet[h].Key == key)
                {
                    _hashSet[h].IsRemoved = true;
                }
                i++;
            }
        }

        public override string ToString()
        {
            var stat = getStat();
            return new StringBuilder()
              .AppendLine("All buckets:       " + _m)
              .AppendLine("Inserted values:   " + stat.insertedValuesCount)
              .AppendLine("Filled buckets:    " + stat.filledBucketsCount)
              .ToString();
        }

        private long hash(long key, long i)
        {
            return _hashFunction.Hash(key, _m, i);
        }
        
        private Statistic getStat()
        {
            var stat = new Statistic();
            for (int i = 0; i < _hashSet.Length; i++)
            {
                var currBucket = _hashSet[i];
                if (currBucket != null)
                {
                    stat.filledBucketsCount ++;
                    stat.insertedValuesCount ++;
                }
            }
            return stat;
        }
        
        private class Bucket
        {
            public long Key { get; set; }
            public bool IsRemoved { get; set; }
        }

        private class Statistic
        {
            public long insertedValuesCount;
            public long filledBucketsCount;
        }

    }
}
