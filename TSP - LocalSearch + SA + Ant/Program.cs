﻿using System;
using System.Runtime.InteropServices;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TSP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> shortestDistances = new List<double>();

            for (int solveId = 0; solveId < 10; solveId++)
            {
                TravellingSalesmanProblem problem = new TravellingSalesmanProblem();
                //problem.FileName = "BR17";
                //problem.FileName = "BAYS29";
                //problem.FileName = "FTV33";
                //problem.FileName = "FTV35";
                problem.FileName = "SWISS42";
                //problem.FileName = "P43";
                //problem.FileName = "FTV44";
                //problem.FileName = "FTV47";
                //problem.FileName = "ATT48";
                //problem.FileName = "RY48P";
                //problem.FileName = "EIL51";
                //problem.FileName = "BERLIN52";
                //problem.FileName = "FT53";
                //problem.FileName = "FTV55";
                //problem.FileName = "FTV64";
                //problem.FileName = "EIL76";
                //problem.FileName = "EIL101";
                var beforeSeconds = DateTime.Now.Ticks;
                //problem.LocalSearch(solveId);
                //problem.Anneal(solveId);
                problem.Ant(solveId);
                var afterSeconds = DateTime.Now.Ticks;
                var timeOfWork = new TimeSpan(afterSeconds - beforeSeconds).TotalSeconds;
                string path = "";
                for (int i = 0; i < problem.CitiesOrder.Count - 1; i++)
                {
                    path += problem.CitiesOrder[i] + " -> ";
                }
                path += problem.CitiesOrder[problem.CitiesOrder.Count - 1];

                Console.WriteLine("Shortest Route" + (solveId + 1) + ": " + path);

                Console.WriteLine("The shortest distance is: " + Math.Round(problem.ShortestDistance, 2));
                Console.WriteLine("Time of work: " + Math.Round(timeOfWork, 2));
                shortestDistances.Add(problem.ShortestDistance);
            }

            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("The shortest of the shortest: " + Math.Round(shortestDistances.Min(), 2));
            Console.WriteLine("-------------------------------------------------");

            //var beforeSeconds1 = DateTime.Now.Ticks;
            //Thread.Sleep(10000);
            //var afterSeconds1 = DateTime.Now.Ticks;
            //var timeOfWork1 = new TimeSpan(afterSeconds1 - beforeSeconds1).TotalSeconds;
            //Console.WriteLine(timeOfWork1);

            Console.ReadLine();
        }
    }
}
