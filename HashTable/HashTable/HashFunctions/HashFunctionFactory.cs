﻿using System;

namespace HashTable.HashFunctions
{
    public class HashFunctionFactory
    {
        public static IHashFunction Create(HashFunctionType hashtype)
        {
            IHashFunction basicHash = null;
            if (hashtype == HashFunctionType.Divide)
            {
                basicHash = new DivideHashFunction();
            }
            if (hashtype == HashFunctionType.Multiply)
            {
                basicHash = new MultiplyHashFunction();
            }

            return basicHash;   
        }
    }
}
