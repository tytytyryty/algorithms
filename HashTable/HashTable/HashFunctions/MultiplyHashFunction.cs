﻿using System;

namespace HashTable.HashFunctions
{
    public class MultiplyHashFunction : IHashFunction
    {
        public long Hash(long key, long m)
        {
            //h(k) = (kAmod1)m, 0<A<1, m=2^p, A=0.618034 should be slow
            return (long)Math.Floor(Math.Abs(key) * 0.618034f % 1 * m);
        }

        public long Hash(long key, long m, long i)
        {
            return Hash(key, m);
        }
    }
}
