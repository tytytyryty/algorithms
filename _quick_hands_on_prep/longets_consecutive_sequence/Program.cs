﻿    var result = LongestConsecutive(new int[] {0,3,7,2,5,8,4,6,0,1});
    Console.WriteLine(result);

    static int LongestConsecutive(int[] nums) {
        var numsMap = new Dictionary<int, bool>();
        for (int i=0; i<nums.Length; i++) {
            numsMap[nums[i]] = false;
        }
        
        var longestStreek = 0;
        var currentStreek = 0;
        foreach (var i in numsMap) {
            if (numsMap[i.Key] == true) {
                continue;
            }
            
            currentStreek = 1;
            numsMap[i.Key] = true;
            
            var previous = i.Key - 1;
            while (numsMap.ContainsKey(previous)) {
                currentStreek++;
                numsMap[previous] = true;
                previous--;
            }
            var next = i.Key + 1;
            while (numsMap.ContainsKey(next)) {
                currentStreek++;
                numsMap[next] = true;
                next++;
            }
            longestStreek = Math.Max(currentStreek, longestStreek);
        }
        
        return longestStreek;
        /*
        //solution with a HashSet, checking if previous number not in it, 
        //then current number starts new increasing sequence
        //for other numbers higher then current, inner loop will not run, as they will have previous value in a 

        var numsMap = new HashSet<int>();
        for (int i=0; i<nums.Length; i++) {
            numsMap.Add(nums[i]);
        }
        
        var longestStreek = 0;
        var currentStreek = 0;
        foreach (var i in numsMap) {
            if (!numsMap.Contains(i-1)) {
                var next = i + 1;
                currentStreek = 1;
                while (numsMap.Contains(next)) {
                    currentStreek++;
                    next++;
                }
            
                longestStreek = Math.Max(currentStreek, longestStreek);
            }
        }
        
        return longestStreek;
        */
    }