﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    interface IHashSet
    {
        void Add(long key);
        void Remove(long key);
        bool Contains(long key);
    }
}
