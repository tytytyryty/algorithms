#include "MazePrinter.h"
#include <iostream>
#include <iomanip>
using namespace std;

MazePrinter::MazePrinter()
{
}


MazePrinter::~MazePrinter()
{
}

void MazePrinter::Print(Maze maze)
{
	std::string output;
	const int otherSymbolsMultiplier = 10;
	output.reserve(maze.rows * maze.cols * otherSymbolsMultiplier);

	for (int j = 0; j < maze.cols; j++)
	{
		if (maze(0, j).hasTopWall)
		{
			if (j == maze.cols - 1)
			{
				output += "----";
			}
			else 
			{
				output += "---";
			}
		}
	}
	output += '\n';

	for (int i = 0; i < maze.rows; i++)
	{
		for (int j = 0; j < maze.cols; j++)
		{
			if (j == 0 && maze(i, j).hasLeftWall == true)
			{
				output += "|";
			}

			if (maze(i, j).hasPlayer)
			{
				if (maze(i, j).isEnter)
					output += ">?";
				else if (maze(i, j).isExit)
					output += "?>";
				else
					output += "? ";
			}
			else if (maze(i, j).isEnter)
			{
				if (maze(i, j).hasPath) 
				{
					output += ">.";
				}
				else
				{
					output += "> ";
				}
			}
			else if (maze(i, j).isExit) 
			{
				output += " >";
			}
			else if (maze(i, j).hasPath)
			{
				output += " .";
			}
			else 
			{
				output += "  ";
			}
			
			if (maze(i, j).hasRightWall == true)
			{
				output += "|";
			}
			else 
			{
				output += " ";
			}
		}
		output += '\n';
		
		for (int j = 0; j < maze.cols; j++)
		{
			if (maze(i, j).hasBottomWall == true)
			{
				if (j == maze.cols - 1)
				{
					output += "----";
				}
				else
				{
					output += "---";
				}
			}
			else
			{
				if (j == maze.cols - 1)
				{
					output += "-  -";
				}
				else
				{
					output += "-  ";
				}
			}
		}
		output += '\n';
	}

	cout << output;
}
