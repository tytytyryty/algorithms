#include "MazeSolver.h"
#include <stack>
#include "NeighbourCell.h"
using namespace std;



MazeSolver::MazeSolver()
{
}


MazeSolver::~MazeSolver()
{
}


MazePath MazeSolver::Solve(Maze maze) 
{
	stack<Point> pointsStack;
	pointsStack.push(maze.enterPoint);

	while (!pointsStack.empty())
	{
		Point currentPoint = pointsStack.top();
		maze(currentPoint).hasPlayer = true;
		maze(currentPoint).hasPath = true;
		maze(currentPoint).isVisited = true;
		(*interimSolvingStep)(maze);

		if (maze(currentPoint).isExit)
		{
			return MazePath();
		}

		pair<NeighbourCell*, int> neighboursToGoResult = getNeighboursToGo(currentPoint, maze);
		NeighbourCell* neighboursToGo = neighboursToGoResult.first;
		int neighboursToGoCount = neighboursToGoResult.second;

		if (neighboursToGoCount > 0)
		{
			int choosedNeightbourIndex = rand() % neighboursToGoCount;
			NeighbourCell choosedNeighbour = neighboursToGo[choosedNeightbourIndex];
			
			pointsStack.push(choosedNeighbour.point);

			//why can do here?
			delete[] neighboursToGo;
		}
		else
		{
			pointsStack.pop();
			maze(currentPoint).hasPath = false;
		}

		maze(currentPoint).hasPlayer = false;
	}

	return EmptyMazePath();
}

void MazeSolver::OnInterimSolvingStep(void (*interimSolvingStep)(Maze maze)) {
	this->interimSolvingStep = interimSolvingStep;
}


pair<NeighbourCell*, int> MazeSolver::getNeighboursToGo(Point const& point, Maze& maze)
{
	NeighbourCell* neighboursToGo = new NeighbourCell[CELL_NEIGHBOURS_COUNT];
	int neighboursToGoCount = 0;

	MazeCell currentCell = maze.index(point.row, point.col);

	Point topPoint = Point(point.row - 1, point.col);
	Point bottomPoint = Point(point.row + 1, point.col);
	Point leftPoint = Point(point.row, point.col - 1);
	Point rightPoint = Point(point.row, point.col + 1);

	if (topPoint.row >= 0 && !currentCell.hasTopWall && !maze(topPoint).isVisited)
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(topPoint, Direction::top);
		neighboursToGoCount++;
	}
	if (bottomPoint.row < maze.rows && !currentCell.hasBottomWall && !maze(bottomPoint).isVisited)
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(bottomPoint, Direction::bottom);
		neighboursToGoCount++;
	}
	if (leftPoint.col >= 0 && !currentCell.hasLeftWall && !maze(leftPoint).isVisited)
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(leftPoint, Direction::left);
		neighboursToGoCount++;
	}
	if (rightPoint.col < maze.cols && !currentCell.hasRightWall && !maze(rightPoint).isVisited)
	{
		neighboursToGo[neighboursToGoCount] = NeighbourCell(rightPoint, Direction::right);
		neighboursToGoCount++;
	}

	return pair<NeighbourCell*, int>(neighboursToGo, neighboursToGoCount);
}