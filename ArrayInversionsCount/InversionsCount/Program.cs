﻿using Sorting;
using System;
using System.IO;
using System.Linq;

namespace InversionsCount
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = GetArrayToCountInversions();

            var mergeSort = new MergeSort();
            mergeSort.Sort(array);
            Console.WriteLine(mergeSort.NumberOfInversions);
            Console.ReadLine();
        }

        private static int[] GetArrayToCountInversions()
        {
            string[] lines = File.ReadAllLines("input.txt");
            var generalInfo = lines[0].Split(new[] { ' ' });
            var amountOfUsers = int.Parse(generalInfo[0]);
            var amountOfFilms = int.Parse(generalInfo[1]);

            int user1Index = 1;
            int user2Index = 2;

            var arrayToCountInversions = new int[amountOfFilms];
            var user1Ratings = lines[user1Index].Split(new[] { ' ' }).ToList();
            user1Ratings.RemoveAt(0);
            var user2Ratings = lines[user2Index].Split(new[] { ' ' }).ToList();
            user2Ratings.RemoveAt(0);

            for (int i = 0; i < arrayToCountInversions.Length; i++)
            {
                var filmNumber = (i + 1).ToString();

                var user1RatingIndex = user1Ratings.IndexOf(filmNumber);
                var user2Rating = user2Ratings[user1RatingIndex];

                arrayToCountInversions[i] = int.Parse(user2Rating);
            }

            return arrayToCountInversions;
        }
    }
}
