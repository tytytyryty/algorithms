﻿
using NUnit.Framework;

namespace BinaryTree
{
    [TestFixture]
    class BinarySearchTreeTests 
    {
        [Test]
        public void GetInSortedOrderWorksCorrectly()
        {
            //arrange
            var bst = new BinarySearchTree {
                Root = new BinaryTreeNode(
                   5,
                   new Left(3,
                       new Left(2),
                       new Right(4)),
                   new Right(7,
                       new Left(6),
                       new Right(8))
                   )};
            var expectedArr = new[] { 2, 3, 4, 5, 6, 7, 8 };

            //act
            var result = bst.GetInSortedOrder();

            //assert
            CollectionAssert.AreEqual(expectedArr, result);
        }

        [Test]
        public void FromBinaryTreeWorksCorrectly()
        {
            //arrange
            var initialBinaryTree = new BinaryTree {
                Root = new BinaryTreeNode(
                        1,
                        new Left(4,
                            new Left(6,
                                new Left(10)),
                            new Right(7,
                                new Right(8))
                            ),
                        new Right(2,
                            new Left(5),
                            new Right(3,
                                new Left(9))
                            )
                        )};

            var expectedBinarySearchTree = new BinarySearchTree {
                Root = new BinaryTreeNode(
                        6,
                        new Left(3,
                            new Left(2,
                                new Left(1)),
                            new Right(4,
                                new Right(5))
                            ),
                        new Right(8,
                            new Left(7),
                            new Right(10,
                                new Left(9))
                            )
                        )};

            //act
            var result = BinarySearchTree.FromBinaryTree(initialBinaryTree);

            //assert
            Assert.IsTrue(result.Equals(expectedBinarySearchTree));
        }
    }
}
