﻿using System.Collections.Generic;
using System.Linq;

namespace Sorting
{
    class HeapSort : ISortable
    {
        public IEnumerable<int> Sort(IEnumerable<int> array)
        {
            var heapArr = new HeapArr() { Array = array.ToArray() };
            BuildMaxHeap(heapArr);
            for (int i = heapArr.Array.Length - 1; i >= 1; i--)
            {
                int tmp = heapArr.Array[0];
                heapArr.Array[0] = heapArr.Array[i];
                heapArr.Array[i] = tmp;
                heapArr.HeapSize--;
                MaxHeapify(heapArr, 0);
            }
            return heapArr.Array;
        }

        public class HeapArr
        {
            public int HeapSize { get; set; }

            public int[] Array { get; set; } 
        }

        private int Parent(int i)
        {
            i++;
            return i / 2 - 1;
        }

        private int Left(int i)
        {
            i++;
            return 2 * i - 1;
        }

        private int Right(int i)
        {
            i++;
            return 2 * i;
        }

        private void MaxHeapify(HeapArr heapArr, int i)
        {
            int l = Left(i);
            int r = Right(i);
            int largest;
            if (l < heapArr.HeapSize && heapArr.Array[l] > heapArr.Array[i])
            {
                largest = l;
            }
            else
            {
                largest = i;
            }
            if (r < heapArr.HeapSize && heapArr.Array[r] > heapArr.Array[largest])
            {
                largest = r;
            }

            if (largest != i)
            {
                int tmp = heapArr.Array[i];
                heapArr.Array[i] = heapArr.Array[largest];
                heapArr.Array[largest] = tmp;
                MaxHeapify(heapArr, largest);
            }
        }

        private void BuildMaxHeap(HeapArr heapArr)
        {
            heapArr.HeapSize = heapArr.Array.Length;
            for (int i = heapArr.Array.Length / 2 - 1; i >= 0; i--)
            {
                MaxHeapify(heapArr, i);
            }
        }
    }
}
