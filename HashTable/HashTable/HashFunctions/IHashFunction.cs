﻿
namespace HashTable.HashFunctions
{
    public interface IHashFunction
    {
        long Hash(long key, long m);
    }
}
