﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeConsoleOutputLib;

namespace TreeConsoleOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 8, 3, 4, 5, 6, 7, 8, 9, 5 };  
            PrintArray(array);

            Console.WriteLine();

            var outputHelper = new TreeConsoleOutputHelper(array, ArrowsOutputType.Before, 1);
            outputHelper.Output();

            Console.Read();
        }

        private static void PrintArray(int [] arr)
        {
            foreach (var i in arr)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }
    }
}
