﻿
namespace BinaryTree
{
    class Left : BinaryTreeNode
    {
        public Left(int value) : base(value, null, null) { }

        public Left(int value, Left left) : base(value, left, null) { }

        public Left(int value, Right right) : base(value, null, right) { }

        public Left(int value, Left left, Right right) : base(value, left, right) { }
    }
}
