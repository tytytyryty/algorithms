﻿using HashTable.HashFunctions;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Text;

namespace HashTable
{
    class ChainingHashSet : IHashSet
    {
        private LinkedList<long>[] _hashSet;
        private long _m;
        private HashFunctionType _hashFunctionType;
        private IHashFunction _hashFunction;
        
        public ChainingHashSet(long size, HashFunctionType hashFunctionType)
        {
            _m = size;
            _hashSet = new LinkedList<long>[size];
            _hashFunctionType = hashFunctionType;

            _hashFunction = HashFunctionFactory.Create(hashFunctionType);
        }

        public void Add(long key)
        {
            var h = hash(key);
            if (_hashSet[h] == null)
            {
                _hashSet[h] = new LinkedList<long>();
            }
            _hashSet[h].AddLast(key);
        }

        public bool Contains(long key)
        {
            var h = hash(key);
            return _hashSet[h] != null && _hashSet[h].Contains(key);
        }

        public void Remove(long key)
        {
            var h = hash(key);
            _hashSet[h].Remove(key);
        }

        public override string ToString()
        {
            var stat = getStat();
            return new StringBuilder()
              .AppendLine("All buckets:       " + _m)
              .AppendLine("Inserted values:   " + stat.insertedValuesCount)
              .AppendLine("Filled buckets:    " + stat.filledBucketsCount)
              .AppendLine("Max bucket length: " + stat.maxBucketLength)
              .ToString();
        }

        private long hash(long key)
        {
            return _hashFunction.Hash(key, _m);
        }
        
        private Statistic getStat()
        {
            var stat = new Statistic();
            for (int i = 0; i < _hashSet.Length; i++)
            {
                var currBucket = _hashSet[i];
                if (currBucket != null)
                {
                    stat.filledBucketsCount ++;
                    stat.insertedValuesCount += currBucket.Count;
                    if (currBucket.Count > stat.maxBucketLength)
                    {
                        stat.maxBucketLength = currBucket.Count;
                    }
                }
            }
            return stat;
        }
        
        private class Statistic
        {
            public long insertedValuesCount;
            public long filledBucketsCount;
            public long maxBucketLength;
        }

    }
}
