#include "MazeCell.h"
#include "Point.h"
#include <cstddef>
#include <memory>
using namespace std;

#pragma once
class Maze 
{
	private:
		shared_ptr<MazeCell[]> maze;
		int size;
		void setMazeEnter(Point* enterPointPtr);
		void setMazeExit(Point* exitPointPtr);
	public:
		//TODO: encapsulation re-thing
		Maze(int rows, int cols, Point* enterPointPtr = NULL, Point* exitPointPtr = NULL);
		~Maze();
		int rows{};
		int cols;
		Point enterPoint;
		Point exitPoint;
		MazeCell& index(int i, int j) const;
		MazeCell& operator()(int i, int j) const;
		MazeCell& index(Point point) const;
		MazeCell& operator()(Point point) const;
};