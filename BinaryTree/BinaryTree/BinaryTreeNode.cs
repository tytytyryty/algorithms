﻿
namespace BinaryTree
{
    class BinaryTreeNode
    {
        //public bool IsRoot { get; set; }
        public BinaryTreeNode Left { get; set; }
        public BinaryTreeNode Right { get; set; }
        public int Value { get; set; }
        public NodeColor Color { get; set; }

        public BinaryTreeNode(int value) : this(value, null, null)
        {
        }

        public BinaryTreeNode(int value, Left left, Right right)
        {
            Value = value;
            Left = left;
            Right = right;
        }
    }
}
