﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using QueensLocalSearch;

namespace WpfApplication2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int _boardLength = 8;
        private static double cellSize = 50.0;
        private static Color lightCellColor = Color.FromRgb(80, 24, 0);
        private static Color darkCellColor = Color.FromRgb(255,244,224);

        List<QueenImage> _queenImages = new List<QueenImage>();

        public MainWindow()
        {
            InitializeComponent();
            initBoard();
        }

        private void initBoard()
        {
            for (int i = 0; i < _boardLength; i++)
            {
                for (int j = 0; j < _boardLength; j++)
                {
                    Rectangle currentCell = new Rectangle {Height = cellSize, Width = cellSize};
                    currentCell.Name = "c" + i + j;
                    currentCell.SetValue(Canvas.TopProperty, i*cellSize);
                    currentCell.SetValue(Canvas.LeftProperty, j*cellSize);
                    var fillColor = new SolidColorBrush(Colors.Aqua);
                    if ((i + j) % 2 == 0)
                    {
                        fillColor.Color = darkCellColor;
                    }
                    else
                    {
                        fillColor.Color = lightCellColor;
                    }
                    currentCell.Fill = fillColor;
                    CanvasBoard.Children.Add(currentCell);
                }
            }
        }
        private void outputPosition(object o, char[,] position)
        {
            int speed = 0;

            Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    for (int i = 0; i < _boardLength; i++)
                    {
                        for (int j = 0; j < _boardLength; j++)
                        {
                            var queenImage = _queenImages.Find((q) => (q.Row == i && q.Column == j));

                            if (position[i, j] == '.' && queenImage != null)
                            {
                                removeQueenImage(queenImage);
                            }
                            
                            if (position[i, j] == 'Q' && queenImage == null)
                            {
                                addQueenImage(i, j);
                            }
                            
                        }
                    }

                    speed = Convert.ToInt32(SliderSpeed.Value);

                }));

            Thread.Sleep(speed);
        }

        private void addQueenImage(int row, int column)
        {
            Image image = new Image();
            image.Source = new BitmapImage(new Uri("FerzW.gif", UriKind.Relative));// _queenImage.Source;
            image.Height = 50;//_queenImage.Height;
            image.Width = 50;//_queenImage.Width;
            image.SetValue(Canvas.TopProperty, row * cellSize);
            image.SetValue(Canvas.LeftProperty, column * cellSize);
            CanvasBoard.Children.Add(image);
            _queenImages.Add(new QueenImage()
            {
                Image = image,
                Row = row,
                Column = column
            });
        }

        private void removeQueenImage(QueenImage queenImage)
        {
            CanvasBoard.Children.Remove(queenImage.Image);
            _queenImages.Remove(queenImage); 
        }

        private void outputSuccess()
        {
            Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    BtnBacktracking.IsEnabled = BtnLocalSearch.IsEnabled = true;

                    LblSuccess.Visibility = Visibility.Visible;
                    LblSuccess.BeginAnimation(WidthProperty, new DoubleAnimation(0, 165, TimeSpan.FromSeconds(1)));
                    LblSuccess.BeginAnimation(OpacityProperty, new DoubleAnimation(0, 1, TimeSpan.FromSeconds(2)));
                    
                }));
        }

        private void solve(SolvingAlgoritm algoritm)
        {
            foreach (var queenImage in _queenImages)
            {
                CanvasBoard.Children.Remove(queenImage.Image);
            }
            _queenImages.Clear();
            BtnBacktracking.IsEnabled = BtnLocalSearch.IsEnabled = false;
            LblSuccess.Visibility = Visibility.Hidden;

            QueensProblem problem = new QueensProblem();
            problem.StartFromRow = ComboStartCell.SelectedIndex;
            problem.PositionChanged += outputPosition;
            problem.ProblemSolved += outputSuccess;

            ThreadStart solveFunction;
            if (algoritm == SolvingAlgoritm.Backtracking)
            {
                solveFunction = problem.Backtracking;
            }
            else
            {
                solveFunction = problem.LocalSearch;
            }
            var thread = new Thread(solveFunction);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void Backtracking_OnClick(object sender, RoutedEventArgs e)
        {
            solve(SolvingAlgoritm.Backtracking);
        }

        private void LocalSearch_OnClick(object sender, RoutedEventArgs e)
        {
            solve(SolvingAlgoritm.LocalSearch);
        }

        public enum SolvingAlgoritm
        {
            Backtracking,
            LocalSearch
        }
    }

    public class QueenImage
    {
        public Image Image;
        public int Row;
        public int Column;
    }

}
