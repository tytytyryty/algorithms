﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    class MergeSort : ISortable
    {
        public IEnumerable<int> Sort(IEnumerable<int> array)
        {
            var arrayToSort = array.ToArray();
            return MergeSortRun(arrayToSort);
        }

        public int[] MergeSortRun(int[] array)
        {
            int[] leftArray;
            int[] rightArray;
            if (array.Length > 1)
            {
                leftArray = MergeSortRun(array.Take(array.Length / 2).ToArray());
                rightArray = MergeSortRun(array.Skip(array.Length / 2).ToArray());
                return Merge(leftArray, rightArray);
            }
            return array;
        }

        static int[] Merge(int[] leftArray, int[] rightArray)
        {
            int n = leftArray.Length + rightArray.Length;
            int[] outputArray = new int[n];
            int i = 0;
            int j = 0;
            for (int k = 0; k < n; k++)
            {
                //если дошли до конца первого массива
                if (i == leftArray.Length)
                {
                    outputArray[k] = rightArray[j];
                    j++;
                }
                //если дошли до конца второго массива
                else if (j == rightArray.Length)
                {
                    outputArray[k] = leftArray[i];
                    i++;
                }
                //если не дошли конца ни одного из массивов
                else
                {
                    if (leftArray[i] < rightArray[j])
                    {
                        outputArray[k] = leftArray[i];
                        i++;
                    }
                    else
                    {
                        outputArray[k] = rightArray[j];
                        j++;
                    }    
                }
            }
            return outputArray;
        }
    }
}
